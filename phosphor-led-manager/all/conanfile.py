"""phosphor-led-manager构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-led-manager"
    url = "https://github.com/openbmc/phosphor-led-manager.git"
    license = "Apache 2.0"
    description = ("This project manages LED groups on dbus.")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "use_json": [True, False],
        "use_lamp_test": [True, False],
        "monitor_operational_status": [True, False],
        "persistent_led_asserted": [True, False],
    }
    default_options = {
        "use_json": True,
        "use_lamp_test": True,
        "monitor_operational_status": False,
        "persistent_led_asserted": True,
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["use-json"] = ("enabled" if self.options.use_json else "disabled")
        tc.project_options["use-lamp-test"] = ("enabled" if self.options.use_lamp_test else "disabled")
        tc.project_options["monitor-operational-status"] = ("enabled" if self.options.monitor_operational_status else "disabled")
        tc.project_options["persistent-led-asserted"] = ("enabled" if self.options.persistent_led_asserted else "disabled")
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        pass

    def package_info(self):
        pass
