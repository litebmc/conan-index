"""phosphor-dbus-interfaces构建脚本"""
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git

required_conan_version = ">=2.0.5"


# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-dbus-interfaces"
    url = "https://github.com/openbmc/phosphor-dbus-interfaces.git"
    license = "Apache 2.0"
    description = ("YAML descriptors of standard D-Bus interfaces")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "generate_md": [True, False],
        "libphosphor_dbus": [True, False],
        "data_com_google": [True, False],
        "data_com_ibm": [True, False],
        "data_com_intel": [True, False],
        "data_com_meta": [True, False],
        "data_org_freedesktop": [True, False],
        "data_org_open_power": [True, False],
        "data_xyz_openbmc_project": [True, False]
    }
    default_options = {
        "generate_md": False,
        "libphosphor_dbus":  True,
        "data_com_google": True,
        "data_com_ibm": True,
        "data_com_intel": True,
        "data_com_meta": True,
        "data_org_freedesktop": True,
        "data_org_open_power": True,
        "data_xyz_openbmc_project": True
    }

    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def layout(self):
        basic_layout(self)

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["generate_md"] =  (True if self.options.generate_md else False)
        tc.project_options["libphosphor_dbus"] =  (True if self.options.libphosphor_dbus else False)
        tc.project_options["data_com_google"] =  (True if self.options.data_com_google else False)
        tc.project_options["data_com_ibm"] =  (True if self.options.data_com_ibm else False)
        tc.project_options["data_com_intel"] =  (True if self.options.data_com_intel else False)
        tc.project_options["data_com_meta"] =  (True if self.options.data_com_meta else False)
        tc.project_options["data_org_freedesktop"] =  (True if self.options.data_org_freedesktop else False)
        tc.project_options["data_org_open_power"] =  (True if self.options.data_org_open_power else False)
        tc.project_options["data_xyz_openbmc_project"] =  (True if self.options.data_xyz_openbmc_project else False)
        tc.generate()

    def build(self):
        meson = Meson(self)
        meson.configure()
        meson.install()

    def package(self):
        pass

    def package_info(self):
        self.cpp_info.libs = ["phosphor_dbus"]
        self.cpp_info.include_dirs = ["include"]
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_target_name", "phosphor-dbus-interfaces::phosphor-dbus-interfaces")
        self.cpp_info.set_property("pkg_config_name", "phosphor-dbus-interfaces")
        self.cpp_info.set_property("pkg_config_custom_content", "yamldir=%s/share/phosphor-dbus-yaml/yaml/" % self.package_folder)
