import os
import hashlib
import shutil

from conan import ConanFile
from conan.errors import ConanInvalidConfiguration
from conan.tools.files import chdir, copy, get, download, unzip
from conan.tools.gnu import Autotools, AutotoolsToolchain
from conan.tools.layout import basic_layout
from conan.errors import ConanException

required_conan_version = ">=1.53.0"


class LinuxConan(ConanFile):
    name = "linux"
    description = "Generic Linux kernel"
    license = "GPL-2.0-only"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://www.kernel.org/"
    topics = ("linux")

    package_type = "unknown"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "defconfig": ["ANY"],
        "defsha256": ["ANY"],
        "headers_only": [True, False],
    }
    default_options = {
        "defconfig": "configs/virt_defconfig",
        "defsha256": "c9bf3da855f08ba0e5c8d789301cc66fa7a8ce30ab0be98fb04cbb8fad45d6c7",
        "headers_only": False
    }
    arch_map = {
        "x86": "x86",
        "x86_64": "x86",
        "armv8": "arm64",
        "armv8.3": "arm64",
        "armv6": "arm",
        "armv7": "arm",
        "armv7hf": "arm",
        "armv7s": "arm",
    }
    # def layout(self):
    #     basic_layout(self, src_folder="src")

    def export_sources(self):
        copy(self, "configs/*", self.recipe_folder, self.export_sources_folder)

    @property
    def _is_legacy_one_profile(self):
        return not hasattr(self, "settings_build")

    def build_requirements(self):
        self.tool_requires("bison/3.8.2")

    def validate(self):
        if self.settings.os != "Linux" or (not self._is_legacy_one_profile and self.settings_build.os != "Linux"):
            raise ConanInvalidConfiguration("only support Linux")

        if not self.arch_map.get(str(self.settings.arch)):
            raise ConanInvalidConfiguration(f"arch {self.settings.arch} not supportted")
        defconfig = str(self.options.defconfig)
        if not defconfig.endswith("_defconfig"):
            raise ConanInvalidConfiguration(f"defconfig file ({defconfig}) not endswith _defconfig")

    def filename(self):
        url = self.conan_data["sources"][self.version].get("url", [])
        url_base = url[0] if isinstance(url, (list, tuple)) else url
        if "?" in url_base or "=" in url_base:
            raise ConanException("Cannot deduce file name from the url: '{}'. Use 'filename' "
                                "parameter.".format(url_base))
        return os.path.basename(url_base)

    def source(self):
        # get(self, **self.conan_data["sources"][self.version], strip_root=True)
        download(self, filename=self.filename(), **self.conan_data["sources"][self.version])

    def generate(self):
        tc = AutotoolsToolchain(self)
        env = tc.environment()
        chost = self.buildenv.vars(self).get("CHOST")
        if chost:
            env.define("CROSS_COMPILE", chost + "-")
        env.define("ARCH", self.arch_map.get(str(self.settings.arch)))
        env.unset("CPPFLAGS")
        env.unset("CXXFLAGS")
        env.unset("CFLAGS")
        env.unset("LDFLAGS")
        tc.generate(env)

    def build(self):
        unzip(self, self.filename(), destination=".", keep_permissions=False,
          pattern=None, strip_root=True)
        defconfig = str(self.options.defconfig)
        defconfig = os.path.realpath(defconfig)
        if not os.path.isfile(defconfig):
            raise ConanInvalidConfiguration(f"defconfig file ({defconfig}) not exist")

        sha256 = hashlib.sha256()
        with open(defconfig, 'rb') as f:
            while True:
                data = f.read(4096)
                if not data:
                    break
                sha256.update(data)
        real_sha256 = sha256.hexdigest()
        if real_sha256 != str(self.options.defsha256):
            raise ConanInvalidConfiguration(f"the sha256sum of defconfig file ({defconfig}) not match," \
                                            f"need: {self.options.defsha256}, get: {real_sha256}")

        def_name = os.path.basename(defconfig)
        shutil.copyfile(defconfig, os.path.join("arch/arm64/configs", def_name))
        autotools = Autotools(self)
        if self.options.headers_only:
            autotools.make(target="headers")
        else:
            args = [
                def_name,
                "ARCH=" + self.arch_map.get(str(self.settings.arch))
            ]
            autotools.make(args=args)
            args = [
                "Image",
                "ARCH=" + self.arch_map.get(str(self.settings.arch))
            ]
            autotools.make(args=args)
            args = [
                "modules",
                "ARCH=" + self.arch_map.get(str(self.settings.arch))
            ]
            autotools.make(args=args)
            args = [
                "modules_install",
                "ARCH=" + self.arch_map.get(str(self.settings.arch)),
                "INSTALL_MOD_PATH=" + os.path.join(self.package_folder)
            ]
            autotools.make(args=args)

    def package(self):
        if self.options.headers_only:
            copy(self, "COPYING",
                dst=os.path.join(self.package_folder, "licenses"),
                src=self.source_folder)
            copy(self, "include/*.h",
                dst=self.package_folder,
                src=os.path.join(self.source_folder, "usr"))
        else:
            copy(self, "Image", src=os.path.join(self.build_folder, "arch/arm64/boot"),
                 dst=os.path.join(self.package_folder, "boot"))
            os.chdir(self.package_folder)
            for root, files, _ in os.walk("."):
                for file in files:
                    file = os.path.join(root, file)
                    if os.path.islink(file):
                        os.unlink(file)

    def package_info(self):
        self.cpp_info.bindirs = []
        self.cpp_info.libdirs = []
