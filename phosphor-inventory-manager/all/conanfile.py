"""phosphor-inventory-manager构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-inventory-manager"
    url = "https://github.com/openbmc/phosphor-inventory-manager.git"
    license = "Apache 2.0"
    description = ("User Management")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "associations": [True, False],
        "YAML_PATH": ["ANY"],
        "IFACES_PATH": ["ANY"],
    }
    default_options = {
        "associations": True,
        "YAML_PATH": "/usr/share/phosphor-inventory-manager",
        "IFACES_PATH": "",
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def export_sources(self):
        files.export_conandata_patches(self)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["associations"] = ("enabled" if self.options.associations else "disabled")
        tc.project_options["YAML_PATH"] = str(self.options.YAML_PATH)
        tc.project_options["IFACES_PATH"] = str(self.options.IFACES_PATH)
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        pass

    def package_info(self):
        pass
