"""phosphor-host-ipmid构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-host-ipmid"
    url = "https://github.com/openbmc/phosphor-host-ipmid.git"
    license = "Apache 2.0"
    description = ("OpenBMC PID-based Thermal Control Daemon.")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"
    options = {
        "boot_flag_safe_mode_support": [True, False],
        "i2c_whitelist_check": [True, False],
        "softoff": [True, False],
        "softoff_busname": ["ANY"],
        "softoff_objpath": ["ANY"],
        "ipmi_sms_atn_ack_timeout_secs": ["ANY"],
        "ipmi_host_shutdown_complete_timeout_secs": ["ANY"],
        "host_inband_request_dir": ["ANY"],
        "host_inband_request_file": ["ANY"],
        "board_sensor": ["ANY"],
        "system_sensor": ["ANY"],
        "control_host_busname": ["ANY"],
        "control_host_obj_mgr": ["ANY"],
        "host_name": ["ANY"],
        "power_reading_sensor": ["ANY"],
        "host_ipmi_lib_path": ["ANY"],
        "update_functional_on_fail": [True, False],
        "libuserlayer": [True, False],
        "transport_oem": [True, False],
        "ipmi_whitelist": [True, False],
        "whitelist_conf": ["ANY"],
        "entity_manager_decorators": [True, False],
        "dynamic_sensors": [True, False],
        "dynamic_sensors_write": [True, False],
        "hybrid_sensors": [True, False],
        "sensors_oem": [True, False],
        "sensors_cache": [True, False],
        "shortname_remove_suffix": [True, False],
        "shortname_replace_words": [True, False],
        "sensor_yaml_gen": ["ANY"],
        "invsensor_yaml_gen": ["ANY"],
        "fru_yaml_gen": ["ANY"],
        "get_dbus_active_software": [True, False],
        "fw_ver_regex": ["ANY"],
        "matches_map": ["ANY"],
        "dynamic_storages_only": [True, False],
        "open_power": [True, False],
    }
    default_options = {
        "boot_flag_safe_mode_support": True,
        "i2c_whitelist_check": True,
        "softoff": True,
        "softoff_busname": "xyz.openbmc_project.Ipmi.Internal.SoftPowerOff",
        "softoff_objpath": "/xyz/openbmc_project/ipmi/internal/soft_power_off",
        "ipmi_sms_atn_ack_timeout_secs": 3,
        "ipmi_host_shutdown_complete_timeout_secs": 2700,
        "host_inband_request_dir": "/run/openbmc/",
        "host_inband_request_file": "host@%u-request",
        "board_sensor": "/xyz/openbmc_project/inventory/system/chassis/motherboard",
        "system_sensor": "/xyz/openbmc_project/inventory/system",
        "control_host_busname": "xyz.openbmc_project.Control.Host",
        "control_host_obj_mgr": "/xyz/openbmc_project/control",
        "host_name": "host",
        "power_reading_sensor": "/usr/share/ipmi-providers/power_reading.json",
        "host_ipmi_lib_path": "/usr/lib/ipmid-providers/",
        "update_functional_on_fail": False,
        "libuserlayer": True,
        "transport_oem": False,
        "ipmi_whitelist": True,
        "whitelist_conf": "host-ipmid-whitelist.conf",
        "entity_manager_decorators": True,
        "dynamic_sensors": False,
        "dynamic_sensors_write": False,
        "hybrid_sensors": False,
        "sensors_oem": False,
        "sensors_cache": False,
        "shortname_remove_suffix": True,
        "shortname_replace_words": False,
        "sensor_yaml_gen": "sensor-example.yaml",
        "invsensor_yaml_gen": "inventory-sensor-example.yaml",
        "fru_yaml_gen": "fru-read-example.yaml",
        "get_dbus_active_software": True,
        "fw_ver_regex": "(\\\\d+)\\\\.(\\\\d+)",
        "matches_map": "1,2,0,0,0,0",
        "dynamic_storages_only": False,
        "open_power": True,
    }

    def configure(self):
        if self.options.open_power:
            self.options["phosphor-dbus-interfaces/*"].data_com_ibm = True

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        tc.cpp_args.append("-Wno-unused-parameter")
        tc.cpp_args.append("-Wno-unused-variable")
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["boot-flag-safe-mode-support"] = ("enabled" if self.options.boot_flag_safe_mode_support else "disabled")
        tc.project_options["i2c-whitelist-check"] = ("enabled" if self.options.i2c_whitelist_check else "disabled")
        tc.project_options["softoff"] = ("enabled" if self.options.softoff else "disabled")
        tc.project_options["softoff-busname"] = str(self.options.softoff_busname)
        tc.project_options["softoff-objpath"] = str(self.options.softoff_objpath)
        tc.project_options["ipmi-sms-atn-ack-timeout-secs"] = int(self.options.ipmi_sms_atn_ack_timeout_secs)
        tc.project_options["ipmi-host-shutdown-complete-timeout-secs"] = int(self.options.ipmi_host_shutdown_complete_timeout_secs)
        tc.project_options["host-inband-request-dir"] = str(self.options.host_inband_request_dir)
        tc.project_options["host-inband-request-file"] = str(self.options.host_inband_request_file)
        tc.project_options["board-sensor"] = str(self.options.board_sensor)
        tc.project_options["system-sensor"] = str(self.options.system_sensor)
        tc.project_options["control-host-busname"] = str(self.options.control_host_busname)
        tc.project_options["control-host-obj-mgr"] = str(self.options.control_host_obj_mgr)
        tc.project_options["host-name"] = str(self.options.host_name)
        tc.project_options["power-reading-sensor"] = str(self.options.power_reading_sensor)
        tc.project_options["host-ipmi-lib-path"] = str(self.options.host_ipmi_lib_path)
        tc.project_options["update-functional-on-fail"] = ("enabled" if self.options.update_functional_on_fail else "disabled")
        tc.project_options["libuserlayer"] = ("enabled" if self.options.libuserlayer else "disabled")
        tc.project_options["transport-oem"] = ("enabled" if self.options.transport_oem else "disabled")
        tc.project_options["ipmi-whitelist"] = ("enabled" if self.options.ipmi_whitelist else "disabled")
        tc.project_options["whitelist-conf"] = str(self.options.whitelist_conf)
        tc.project_options["entity-manager-decorators"] = ("enabled" if self.options.entity_manager_decorators else "disabled")
        tc.project_options["dynamic-sensors"] = ("enabled" if self.options.dynamic_sensors else "disabled")
        tc.project_options["dynamic-sensors-write"] = ("enabled" if self.options.dynamic_sensors_write else "disabled")
        tc.project_options["hybrid-sensors"] = ("enabled" if self.options.hybrid_sensors else "disabled")
        tc.project_options["sensors-oem"] = ("enabled" if self.options.sensors_oem else "disabled")
        tc.project_options["sensors-cache"] = ("enabled" if self.options.sensors_cache else "disabled")
        tc.project_options["shortname-remove-suffix"] = ("enabled" if self.options.shortname_remove_suffix else "disabled")
        tc.project_options["shortname-replace-words"] = ("enabled" if self.options.shortname_replace_words else "disabled")
        tc.project_options["sensor-yaml-gen"] = str(self.options.sensor_yaml_gen)
        tc.project_options["invsensor-yaml-gen"] = str(self.options.invsensor_yaml_gen)
        tc.project_options["fru-yaml-gen"] = str(self.options.fru_yaml_gen)
        tc.project_options["get-dbus-active-software"] = ("enabled" if self.options.get_dbus_active_software else "disabled")
        tc.project_options["fw-ver-regex"] = str(self.options.fw_ver_regex)
        tc.project_options["matches-map"] = str(self.options.matches_map).split(",")
        tc.project_options["dynamic-storages-only"] = ("enabled" if self.options.dynamic_storages_only else "disabled")
        tc.project_options["open-power"] = ("enabled" if self.options.open_power else "disabled")
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        pass

    def package_info(self):
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_file_name", "host-ipmid")
        self.cpp_info.set_property("cmake_target_name", "phosphor-host-ipmid::phosphor-host-ipmid")
        self.cpp_info.set_property("pkg_config_name", "phosphor-host-ipmid")
        self.cpp_info.components["libipmid"].set_property("pkg_config_name", "libipmid")
        self.cpp_info.components["libipmid"].libs = ["ipmid"]
        self.cpp_info.components["libipmid"].names["cmake_find_package"] = "libipmid"
        self.cpp_info.components["libipmid"].names["cmake_find_package_multi"] = "libipmid"

        self.cpp_info.components["libchannellayer"].set_property("pkg_config_name", "libchannellayer")
        self.cpp_info.components["libchannellayer"].libs = ["channellayer"]
        self.cpp_info.components["libchannellayer"].names["cmake_find_package"] = "libchannellayer"
        self.cpp_info.components["libchannellayer"].names["cmake_find_package_multi"] = "libchannellayer"

        if self.options.libuserlayer:
            self.cpp_info.components["libuserlayer"].set_property("pkg_config_name", "libuserlayer")
            self.cpp_info.components["libuserlayer"].libs = ["userlayer"]
            self.cpp_info.components["libuserlayer"].names["cmake_find_package"] = "libuserlayer"
            self.cpp_info.components["libuserlayer"].names["cmake_find_package_multi"] = "libuserlayer"

        if self.options.softoff:
            self.cpp_info.components["softoff-dbus"].set_property("pkg_config_name", "softoff-dbus")
            self.cpp_info.components["softoff-dbus"].libs = ["softoff-dbus"]
            self.cpp_info.components["softoff-dbus"].names["cmake_find_package"] = "softoff-dbus"
            self.cpp_info.components["softoff-dbus"].names["cmake_find_package_multi"] = "softoff-dbus"

