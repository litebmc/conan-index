"""phosphor-bmc-code-mgmt构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class ObmcGpioplusConan(ConanFile):
    name = "gpioplus"
    url = "https://github.com/openbmc/gpioplus.git"
    license = "Apache 2.0"
    description = ("User Management")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "examples": [True, False],
    }
    default_options = {
        "examples": False,
    }
    requires = []

    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def export_sources(self):
        files.export_conandata_patches(self)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["datadir"] = "usr/share"
        tc.project_options["tests"] = "disabled"
        tc.project_options["examples"] = (True if self.options.examples else False)
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", src=os.path.join(self.build_folder, "rootfs"), dst=self.package_folder)
        pass

    def package_info(self):
        self.cpp_info.libs = ["gpioplus"]
        self.cpp_info.include_dirs = ["include"]
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_target_name", "gpioplus::gpioplus")
        self.cpp_info.set_property("pkg_config_name", "gpioplus")
