import os
import hashlib

from conan import ConanFile
from conan.tools.files import get, copy
from conan.errors import ConanInvalidConfiguration
from conan.tools.gnu import Autotools, AutotoolsToolchain

required_conan_version = ">=1.53.0"


class BusyboxConan(ConanFile):
    name = "busybox"
    description = "The Swiss Army Knife of Embedded Linux."
    license = "GPL-2.0"
    homepage = "https://www.busybox.net"
    topics = ("shell", "tools")

    package_type = "application"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "defconfig": ["ANY"],
        "defsha256": ["ANY"]
    }
    default_options = {
        "defconfig": "configs/default_defconfig",
        "defsha256": "94376804aaa844e0276ebc5c4247cdcff8ac3d293d78fae91b4dee58979633d3",
    }

    @property
    def _settings_build(self):
        return getattr(self, "settings_build", self.settings)

    def export_sources(self):
        copy(self, "configs/*", os.path.join(self.recipe_folder), self.export_sources_folder)


    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        tc = AutotoolsToolchain(self)
        env = tc.environment()
        chost = self.buildenv.vars(self).get("CHOST")
        if chost:
            env.define("CROSS_COMPILE", chost + "-")
        env.unset("CPPFLAGS")
        env.unset("CXXFLAGS")
        env.unset("CFLAGS")
        env.unset("LDFLAGS")
        tc.generate(env)

    def validate(self):
        defconfig = str(self.options.defconfig)
        if not defconfig.endswith("_defconfig"):
            raise ConanInvalidConfiguration(f"defconfig file ({defconfig}) not endswith _defconfig")

    def build(self):
        defconfig = str(self.options.defconfig)
        defconfig = os.path.realpath(defconfig)
        if not os.path.isfile(defconfig):
            raise ConanInvalidConfiguration(f"defconfig file ({defconfig}) not exist")

        sha256 = hashlib.sha256()
        with open(defconfig, 'rb') as f:
            while True:
                data = f.read(4096)
                if not data:
                    break
                sha256.update(data)
        real_sha256 = sha256.hexdigest()
        if real_sha256 != str(self.options.defsha256):
            raise ConanInvalidConfiguration(f"the sha256sum of defconfig file ({defconfig}) not match," \
                                            f"need: {self.options.defsha256}, get: {real_sha256}")

        dstdir = os.path.join(self.build_folder, "configs")
        dstfile = os.path.basename(defconfig)
        if os.path.join(dstdir, dstfile) != defconfig:
            copy(self, pattern=dstfile, src=os.path.dirname(defconfig), dst=dstdir)
        autotools = Autotools(self)
        args = [
            os.path.basename(defconfig)
        ]
        autotools.make(args=args)
        autotools.make()

    def package(self):
        autotools = Autotools(self)
        args = ["CONFIG_PREFIX=" + self.package_folder]
        autotools.install(args=args)
