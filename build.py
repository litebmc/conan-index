import os
import sys
import yaml
import subprocess

if __name__ == "__main__":
   cwd = os.getcwd()
   for root, _, files in os.walk("."):
        if "config.yml" not in files:
            continue
        file = os.path.join(root, "config.yml")
        with open(file, "r") as fp:
            config = yaml.load(fp, yaml.FullLoader)
        for ver, cfg in config.get("versions", {}).items():
            folder = cfg.get("folder")
            print(folder)
            dir = os.path.join(root, folder)
            os.chdir(dir)
            subprocess.run(f"conan export . --version {ver}", shell=True)
            os.chdir(cwd)
