"""sdbusplus构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "obmc-pldm"
    url = "https://github.com/openbmc/pldm.git"
    license = "Apache 2.0"
    description = ("Platform Level Data Model")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"
    options = {
        "utilities": [True, False],
        "libpldmresponder": [True, False],
        "systemd": [True, False],
        "transport_implementation": ["mctp-demux", "af-mctp"],
        "dbus_timeout_value": ["ANY"],
        "heartbeat_timeout_seconds": ["ANY"],
        "flightrecorder_max_entries": ["ANY"],
        "terminus_id": ["ANY"],
        "terminus_handle": ["ANY"],
        "number_of_request_retries": ["ANY"],
        "instance_id_expiration_interval": ["ANY"],
        "response_time_out": ["ANY"],
        "maximum_transfer_size": ["ANY"],
        "system_specific_bios_json": [True, False],
        "softoff": [True, False],
        "softoff_timeout_seconds": ["ANY"],
        "oem_ibm": [True, False],
        "oem_ibm_dma_maxsize": ["ANY"],
    }
    default_options = {
        "utilities": True,
        "libpldmresponder": True,
        "systemd": True,
        "transport_implementation": "mctp-demux",
        "dbus_timeout_value": 5,
        "heartbeat_timeout_seconds": 120,
        "flightrecorder_max_entries": 10,
        "terminus_id": 1,
        "terminus_handle": 1,
        "number_of_request_retries": 2,
        "instance_id_expiration_interval": 5,
        "response_time_out": 2000,
        "maximum_transfer_size": 4096,
        "system_specific_bios_json": False,
        "softoff": True,
        "softoff_timeout_seconds": 7200,
        "oem_ibm": True,
        "oem_ibm_dma_maxsize": 8384512,
    }

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        tc.cpp_args.append("-Wno-unused-variable")
        tc.cpp_args.append("-Wno-error=unused-parameter")
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["utilities"] = ("enabled" if self.options.utilities else "disabled")
        tc.project_options["libpldmresponder"] = ("enabled" if self.options.libpldmresponder else "disabled")
        tc.project_options["systemd"] = ("enabled" if self.options.systemd else "disabled")
        tc.project_options["transport-implementation"] = str(self.options.transport_implementation)
        tc.project_options["dbus-timeout-value"] = int(self.options.dbus_timeout_value)
        tc.project_options["heartbeat-timeout-seconds"] = int(self.options.heartbeat_timeout_seconds)
        tc.project_options["flightrecorder-max-entries"] = int(self.options.flightrecorder_max_entries)
        tc.project_options["terminus-id"] = int(self.options.terminus_id)
        tc.project_options["terminus-handle"] = int(self.options.terminus_handle)
        tc.project_options["number-of-request-retries"] = int(self.options.number_of_request_retries)
        tc.project_options["instance-id-expiration-interval"] = int(self.options.instance_id_expiration_interval)
        tc.project_options["response-time-out"] = int(self.options.response_time_out)
        tc.project_options["maximum-transfer-size"] = int(self.options.maximum_transfer_size)
        tc.project_options["system-specific-bios-json"] = ("enabled" if self.options.system_specific_bios_json else "disabled")
        tc.project_options["softoff"] = ("enabled" if self.options.softoff else "disabled")
        tc.project_options["softoff-timeout-seconds"] = int(self.options.softoff_timeout_seconds)
        tc.project_options["oem-ibm"] = ("enabled" if self.options.oem_ibm else "disabled")
        tc.project_options["oem-ibm-dma-maxsize"] = int(self.options.oem_ibm_dma_maxsize)
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        pass

    def package_info(self):
        self.cpp_info.libs = ["pldmutils", "pldmresponder"]
        self.cpp_info.include_dirs = ["include"]
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_target_name", "pldm::pldm")
        self.cpp_info.set_property("pkg_config_name", "pldm")

