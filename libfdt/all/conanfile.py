import os
from conan import ConanFile
from conan.tools.layout import basic_layout
from conan.tools.gnu import AutotoolsToolchain, Autotools
from conan.tools.env import VirtualBuildEnv
from conan.tools.files import chdir, get, copy

class LibfdtConan(ConanFile):
    name = "libfdt"
    url = "https://github.com/dgibson/dtc"
    license = "BSD-2"
    description = ("Device Tree Compiler and libfdt")
    # generators = "make"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False]
    }
    default_options = {
        "shared": False
    }

    def layout(self):
        basic_layout(self)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        env = VirtualBuildEnv(self)
        env.generate()
        tc = AutotoolsToolchain(self)
        tc.generate()

    def build(self):
        autotools = Autotools(self)
        args = ["HOME=/"]
        if os.environ.get("VERBOSE"):
            args.append("V=1")
        with chdir(self, self.source_folder):
            autotools.install(target="install-lib", args=args)
            autotools.install(target="install-includes", args=args)

    def package(self):
        with chdir(self, self.package_folder):
            if not self.options.shared:
                self.run("rm lib/libfdt.so*")
                self.run("rm lib/libfdt-*so")
            else:
                self.run("rm lib/libfdt.a")

    def package_info(self):
        if self.options.shared:
            self.cpp_info.libs = ["fdt"]
            self.cpp_info.components["libfdt"].libs = ["fdt"]
        else:
            self.cpp_info.libs = ["libfdt.a"]
            self.cpp_info.components["libfdt"].libs = ["libfdt.a"]

        self.cpp_info.include_dirs = ["include"]
        self.cpp_info.components["libfdt"].libs = ["fdt"]
        self.cpp_info.components["libfdt"].include_dirs = ["include"]
        self.cpp_info.components["libfdt"].set_property("cmake_find_mode", "both")
        self.cpp_info.components["libfdt"].set_property("cmake_file_name", "libfdt")
        self.cpp_info.components["libfdt"].set_property("cmake_target_name", "libfdt::libfdt")
        self.cpp_info.components["libfdt"].set_property("pkg_config_name", "libfdt")