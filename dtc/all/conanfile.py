import os
from conan import ConanFile
from conan.tools.layout import basic_layout
from conan.tools.gnu import AutotoolsToolchain, Autotools
from conan.tools.env import VirtualBuildEnv
from conan.tools.files import chdir, get, copy

class DtcConan(ConanFile):
    name = "dtc"
    url = "https://github.com/dgibson/dtc"
    license = "BSD-2"
    description = ("Device Tree Compiler and libfdt")
    # generators = "make"
    settings = "os", "arch", "compiler", "build_type"

    def layout(self):
        basic_layout(self)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        env = VirtualBuildEnv(self)
        env.generate()
        tc = AutotoolsToolchain(self)
        tc.generate()

    def build(self):
        autotools = Autotools(self)
        args = ["HOME=/"]
        if os.environ.get("VERBOSE"):
            args.append("V=1")
        with chdir(self, self.source_folder):
            autotools.install(target="install-bin", args=args)

    def package_info(self):
        self.buildenv_info.prepend_path("PATH", "bin")