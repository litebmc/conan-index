"""sdbusplus构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-state-manager"
    url = "https://github.com/openbmc/phosphor-state-manager.git"
    license = "Apache 2.0"
    description = ("User Management")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"
    options = {
        "host_busname": ["ANY"],
        "host_objpath": ["ANY"],
        "host_sched_objpath": ["ANY"],
        "hypervisor_busname": ["ANY"],
        "hypervisor_objpath": ["ANY"],
        "chassis_busname": ["ANY"],
        "chassis_objpath": ["ANY"],
        "bmc_busname": ["ANY"],
        "bmc_objpath": ["ANY"],
        "host_state_persist_path": ["ANY"],
        "poh_counter_persist_path": ["ANY"],
        "chassis_state_change_persist_path": ["ANY"],
        "scheduled_host_transition_persist_path": ["ANY"],
        "boot_count_max_allowed": ["ANY"],
        "class_version": ["ANY"],
        "scheduled_host_transition_busname": ["ANY"],
        "warm_reboot": [True, False],
        "force_warm_reboot": [True, False],
        "host_gpios": [True, False],
        "host_gpios_busname": ["ANY"],
        "host_gpios_objpath": ["ANY"],
        "sysfs_secure_boot_path": ["ANY"],
        "sysfs_abr_image_path": ["ANY"],
        "only_run_apr_on_power_loss": [True, False],
        "sysfs_tpm_device_path": ["ANY"],
        "sysfs_tpm_measurement_path": ["ANY"],
        "only_allow_boot_when_bmc_ready": [True, False],
    }
    default_options = {
        "host_busname": "xyz.openbmc_project.State.Host",
        "host_objpath": "/xyz/openbmc_project/state/host",
        "host_sched_objpath": "/xyz/openbmc_project/scheduled/host",
        "hypervisor_busname": "xyz.openbmc_project.State.Hypervisor",
        "hypervisor_objpath": "/xyz/openbmc_project/state/hypervisor",
        "chassis_busname": "xyz.openbmc_project.State.Chassis",
        "chassis_objpath": "/xyz/openbmc_project/state/chassis",
        "bmc_busname": "xyz.openbmc_project.State.BMC",
        "bmc_objpath": "/xyz/openbmc_project/state/bmc",
        "host_state_persist_path": "/var/lib/phosphor-state-manager/host{}-PersistData",
        "poh_counter_persist_path": "/var/lib/phosphor-state-manager/chassis{}-POHCounter",
        "chassis_state_change_persist_path": "/var/lib/phosphor-state-manager/chassis{}-StateChangeTime",
        "scheduled_host_transition_persist_path": "/var/lib/phosphor-state-manager/scheduledHostTransition",
        "boot_count_max_allowed": 3,
        "class_version": 2,
        "scheduled_host_transition_busname": "xyz.openbmc_project.State.ScheduledHostTransition",
        "warm_reboot": True,
        "force_warm_reboot": True,
        "host_gpios": True,
        "host_gpios_busname": "xyz.openbmc_project.State.HostCondition.Gpio",
        "host_gpios_objpath": "/xyz/openbmc_project/Gpios",
        "sysfs_secure_boot_path": "/sys/kernel/debug/aspeed/sbc/secure_boot",
        "sysfs_abr_image_path": "/sys/kernel/debug/aspeed/sbc/abr_image",
        "only_run_apr_on_power_loss": False,
        "sysfs_tpm_device_path": "/sys/firmware/devicetree/base/ahb/apb/bus@1e78a000/i2c-bus@680/tpm@2e",
        "sysfs_tpm_measurement_path": "/sys/class/tpm/tpm0/pcr-sha256/0",
        "only_allow_boot_when_bmc_ready": False,
    }

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def configure(self):
        self.options["libgpiod"].enable_bindings_cxx = True

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        tc.cpp_args.append("-Wno-unused-variable")
        tc.cpp_args.append("-Wno-error=unused-variable")
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["host-busname"] = str(self.options.host_busname)
        tc.project_options["host-objpath"] = str(self.options.host_objpath)
        tc.project_options["host-sched-objpath"] = str(self.options.host_sched_objpath)
        tc.project_options["hypervisor-busname"] = str(self.options.hypervisor_busname)
        tc.project_options["hypervisor-objpath"] = str(self.options.hypervisor_objpath)
        tc.project_options["chassis-busname"] = str(self.options.chassis_busname)
        tc.project_options["chassis-objpath"] = str(self.options.chassis_objpath)
        tc.project_options["bmc-busname"] = str(self.options.bmc_busname)
        tc.project_options["bmc-objpath"] = str(self.options.bmc_objpath)
        tc.project_options["host-state-persist-path"] = str(self.options.host_state_persist_path)
        tc.project_options["poh-counter-persist-path"] = str(self.options.poh_counter_persist_path)
        tc.project_options["chassis-state-change-persist-path"] = str(self.options.chassis_state_change_persist_path)
        tc.project_options["scheduled-host-transition-persist-path"] = str(self.options.scheduled_host_transition_persist_path)
        tc.project_options["boot-count-max-allowed"] = int(self.options.boot_count_max_allowed)
        tc.project_options["class-version"] = int(self.options.class_version)
        tc.project_options["scheduled-host-transition-busname"] = str(self.options.scheduled_host_transition_busname)
        tc.project_options["warm-reboot"] = ("enabled" if self.options.warm_reboot else "disabled")
        tc.project_options["force-warm-reboot"] = ("enabled" if self.options.force_warm_reboot else "disabled")
        tc.project_options["host-gpios"] = ("enabled" if self.options.host_gpios else "disabled")
        tc.project_options["host-gpios-busname"] = str(self.options.host_gpios_busname)
        tc.project_options["host-gpios-objpath"] = str(self.options.host_gpios_objpath)
        tc.project_options["sysfs-secure-boot-path"] = str(self.options.sysfs_secure_boot_path)
        tc.project_options["sysfs-abr-image-path"] = str(self.options.sysfs_abr_image_path)
        tc.project_options["only-run-apr-on-power-loss"] = (True if self.options.only_run_apr_on_power_loss else False)
        tc.project_options["sysfs-tpm-device-path"] = str(self.options.sysfs_tpm_device_path)
        tc.project_options["sysfs-tpm-measurement-path"] = str(self.options.sysfs_tpm_measurement_path)
        tc.project_options["only-allow-boot-when-bmc-ready"] = (True if self.options.only_allow_boot_when_bmc_ready else False)
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        pass

    def package_info(self):
        pass
