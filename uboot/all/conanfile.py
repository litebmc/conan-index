import os
import hashlib

from conan import ConanFile
from conan.errors import ConanInvalidConfiguration
from conan.tools.files import copy, get
from conan.tools.gnu import Autotools, AutotoolsToolchain

required_conan_version = ">=1.53.0"


class UbootConan(ConanFile):
    name = "uboot"
    description = "a boot loader for Embedded boards"
    license = "GPL-2.0+"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://www.denx.de/project/u-boot/"
    topics = ("uboot", "bootloader")

    package_type = "unknown"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "defconfig": ["ANY"],
        "defsha256": ["ANY"],
    }
    default_options = {
        "defconfig": "configs/qemu_arm64_defconfig",
        "defsha256": "8872fef8817c6bedeb023267f4b2a84302c649f32b98fbad3834e728b4cb3ec1",
    }
    def build_requirements(self):
        self.tool_requires("bison/3.8.2")
        self.tool_requires("swig/4.2.1")

    def export_sources(self):
        copy(self, "configs/*", os.path.join(self.recipe_folder), self.export_sources_folder)

    @property
    def _is_legacy_one_profile(self):
        return not hasattr(self, "settings_build")

    def validate(self):
        if self.settings.os != "Linux" or (not self._is_legacy_one_profile and self.settings_build.os != "Linux"):
            raise ConanInvalidConfiguration("only support Linux")

        defconfig = str(self.options.defconfig)
        if not defconfig.endswith("_defconfig"):
            raise ConanInvalidConfiguration(f"defconfig file ({defconfig}) not endswith _defconfig")

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        tc = AutotoolsToolchain(self)
        env = tc.environment()
        chost = self.buildenv.vars(self).get("CHOST")
        if chost:
            env.define("CROSS_COMPILE", chost + "-")
        env.unset("CPPFLAGS")
        env.unset("CXXFLAGS")
        env.unset("CFLAGS")
        env.unset("LDFLAGS")
        tc.generate(env)

    def build(self):
        defconfig = str(self.options.defconfig)
        defconfig = os.path.realpath(defconfig)
        if not os.path.isfile(defconfig):
            raise ConanInvalidConfiguration(f"defconfig file ({defconfig}) not exist")

        sha256 = hashlib.sha256()
        with open(defconfig, 'rb') as f:
            while True:
                data = f.read(4096)
                if not data:
                    break
                sha256.update(data)
        real_sha256 = sha256.hexdigest()
        if real_sha256 != str(self.options.defsha256):
            raise ConanInvalidConfiguration(f"the sha256sum of defconfig file ({defconfig}) not match," \
                                            f"need: {self.options.defsha256}, get: {real_sha256}")

        dstdir = os.path.join(self.build_folder, "configs")
        dstfile = os.path.basename(defconfig)
        if os.path.join(dstdir, dstfile) != defconfig:
            copy(self, pattern=dstfile, src=os.path.dirname(defconfig), dst=dstdir)
        autotools = Autotools(self)
        args = [
            dstfile,
        ]
        autotools.make(args=args)
        args = [
        ]
        autotools.make(args=args)

    def package(self):
        copy(self, "u-boot.bin", src=self.build_folder, dst=self.package_folder)

    def package_info(self):
        self.cpp_info.bindirs = []
        self.cpp_info.libdirs = []
