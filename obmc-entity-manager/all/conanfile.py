"""obmc-entity-manager构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "obmc-entity-manager"
    url = "https://github.com/openbmc/obmc-entity-manager.git"
    license = "Apache 2.0"
    description = ("OpenBMC PID-based Thermal Control Daemon.")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "fru_device": [True, False],
        "fru_device_resizefru": [True, False],
        "validate_json": [True, False],
    }
    default_options = {
        "fru_device": True,
        "fru_device_resizefru": False,
        "validate_json": True,
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/libexec"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        tc.cpp_args.append("-Wno-error=maybe-uninitialized")
        tc.cpp_args.append("-Wno-invalid-constexpr")
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["fru-device"] = (True if self.options.fru_device else False)
        tc.project_options["fru-device-resizefru"] = (True if self.options.fru_device_resizefru else False)
        tc.project_options["validate-json"] = (True if self.options.validate_json else False)
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        pass

    def package_info(self):
        pass
