import copy
from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain, cmake_layout
from conan.tools.files import get

required_conan_version = ">=1.53.0"


class LibVNCServerConan(ConanFile):
    name = "libvncserver"
    description = "This modules offers parsing of ini files from C."
    url = "https://github.com/LibVNC/libvncserver.git"
    homepage = "https://libvnc.github.io/"
    topics = ("tools", "library")
    license = "GPL-2.0"
    settings = "os", "arch", "compiler", "build_type"
    package_type = "library"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "libvncserver_install": [True, False],
        "build_shared_libs": [True, False],
        "with_zlib": [True, False],
        "with_lzo": [True, False],
        "with_jpeg": [True, False],
        "with_png": [True, False],
        "with_libssh2": [True, False],
        "with_threads": [True, False],
        "prefer_win32threads": [True, False],
        "with_gnutls": [True, False],
        "with_openssl": [True, False],
        "with_systemd": [True, False],
        # "with_ffmpeg": [True, False],
        "with_tightvnc_filetransfer": [True, False],
        "with_24bpp": [True, False],
        "with_ipv6": [True, False],
        "with_websockets": [True, False],
        "with_sasl": [True, False],
        "with_examples": [True, False],
        "with_tests": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "libvncserver_install": True,
        "build_shared_libs": True,
        "with_zlib": True,
        "with_lzo": True,
        "with_jpeg": True,
        "with_png": True,
        "with_libssh2": True,
        "with_threads": True,
        "prefer_win32threads": True,
        "with_gnutls": True,
        "with_openssl": True,
        "with_systemd": True,
        # "with_ffmpeg": True,
        "with_tightvnc_filetransfer": True,
        "with_24bpp": True,
        "with_ipv6": True,
        "with_websockets": True,
        "with_sasl": True,
        "with_examples": False,
        "with_tests": False,
    }
    _cmake = None

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            del self.options.fPIC

    def layout(self):
        cmake_layout(self, src_folder=".")

    def requirements(self):
        if self.options.with_zlib:
            self.requires("zlib/[>=1.2.11 <2]")
        if self.options.with_lzo:
            self.requires("lzo/2.10")
        if self.options.with_jpeg:
            self.requires("libjpeg/9f")
        if self.options.with_png:
            self.requires("libpng/1.6.43")
        if self.options.with_libssh2:
            self.requires("libssh2/1.11.0")
        if self.options.with_gnutls:
            self.requires("gnutls/3.8.2")
        if self.options.with_openssl:
            self.requires("openssl/[>=3.0.0]")
        # if self.options.with_ffmpeg:
        #     self.requires("ffmpeg/7.0.1")
        if self.options.with_sasl:
            self.requires("cyrus-sasl/2.1.28")

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["WITH_EXAMPLES"] = False
        tc.variables["WITH_TESTS"] = False
        tc.variables["BUILD_SHARED_LIBS"] = self.options.shared
        tc.variables["LIBVNCSERVER_INSTALL"] = self.options.libvncserver_install
        tc.variables["BUILD_SHARED_LIBS"] = self.options.build_shared_libs
        tc.variables["WITH_ZLIB"] = self.options.with_zlib
        tc.variables["WITH_LZO"] = self.options.with_lzo
        tc.variables["WITH_JPEG"] = self.options.with_jpeg
        tc.variables["WITH_PNG"] = self.options.with_png
        tc.variables["WITH_SDL"] = False
        tc.variables["WITH_GTK"] = False
        tc.variables["WITH_LIBSSH2"] = self.options.with_libssh2
        tc.variables["WITH_THREADS"] = self.options.with_threads
        tc.variables["PREFER_WIN32THREADS"] = self.options.prefer_win32threads
        tc.variables["WITH_GNUTLS"] = self.options.with_gnutls
        tc.variables["WITH_OPENSSL"] = self.options.with_openssl
        tc.variables["WITH_SYSTEMD"] = self.options.with_systemd
        tc.variables["WITH_GCRYPT"] = False
        tc.variables["WITH_FFMPEG"] = False
        tc.variables["WITH_TIGHTVNC_FILETRANSFER"] = self.options.with_tightvnc_filetransfer
        tc.variables["WITH_24BPP"] = self.options.with_24bpp
        tc.variables["WITH_IPV6"] = self.options.with_ipv6
        tc.variables["WITH_WEBSOCKETS"] = self.options.with_websockets
        tc.variables["WITH_SASL"] = self.options.with_sasl
        tc.variables["WITH_EXAMPLES"] = self.options.with_examples
        tc.variables["WITH_TESTS"] = self.options.with_tests
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.include_dirs = ["include"]
        self.cpp_info.libs = ["vncserver", "vncclient"]
