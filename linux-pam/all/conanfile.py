import os
from conan import ConanFile
from conan.tools.layout import basic_layout
from conan.tools.gnu import AutotoolsToolchain, Autotools
from conan.tools.scm import Git
from conan.tools.files import chdir

class LinuxPamConan(ConanFile):
    name = "linux-pam"
    url = "https://github.com/linux-pam/linux-pam.git"
    homepage = "https://github.com/linux-pam/linux-pam.git"
    license = "linux-pam"
    description = ("Linux Pluggable Authentication Modules (PAM)")

    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "AutotoolsToolchain"

    def layout(self):
        basic_layout(self)

    def build_requirements(self):
        self.tool_requires("autoconf/2.71")
        self.tool_requires("automake/1.16.5")
        self.tool_requires("libtool/2.4.7")
        self.tool_requires("pkgconf/2.0.3")
        self.tool_requires("gettext/0.22.5")
        self.tool_requires("xz_utils/5.4.5")
        self.tool_requires("flex/2.6.4")
        self.tool_requires("bison/3.8.2")

    def source(self):
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def generator(self):
        tc = AutotoolsToolchain(self)
        tc.generate()

    def build(self):
        with chdir(self, self.source_folder):
            print(os.environ.get("PATH"))
            self.run("./autogen.sh")

        args = ["--includedir=${prefix}/include/security", "--disable-doc", "--disable-rpath", "--disable-nis"]
        autotools = Autotools(self)
        autotools.configure(args=args)
        autotools.make()
        autotools.install()

    def package(self):
        pass

    def package_info(self):
        self.cpp_info.libs = ["pam", "pamc", "pam_misc"]
        self.cpp_info.include_dirs = ["include", "include/security"]
        self.cpp_info.components["pam"].libs = ["pam"]
        self.cpp_info.components["pam"].include_dirs = ["include"]
        self.cpp_info.components["pam"].set_property("cmake_find_mode", "both")
        self.cpp_info.components["pam"].set_property("cmake_file_name", "pam")
        self.cpp_info.components["pam"].set_property("cmake_target_name", "pam::pam")
        self.cpp_info.components["pam"].set_property("pkg_config_name", "pam")
        self.cpp_info.components["pam_misc"].libs = ["pam_misc"]
        self.cpp_info.components["pam_misc"].include_dirs = ["include"]
        self.cpp_info.components["pam_misc"].set_property("cmake_find_mode", "both")
        self.cpp_info.components["pam_misc"].set_property("cmake_file_name", "pam_misc")
        self.cpp_info.components["pam_misc"].set_property("cmake_target_name", "pam_misc::pam_misc")
        self.cpp_info.components["pam_misc"].set_property("pkg_config_name", "pam_misc")

        self.cpp_info.components["pamc"].libs = ["pamc"]
        self.cpp_info.components["pamc"].include_dirs = ["include"]
        self.cpp_info.components["pamc"].set_property("cmake_find_mode", "both")
        self.cpp_info.components["pamc"].set_property("cmake_file_name", "pamc")
        self.cpp_info.components["pamc"].set_property("cmake_target_name", "pamc::pamc")
        self.cpp_info.components["pamc"].set_property("pkg_config_name", "pamc")
