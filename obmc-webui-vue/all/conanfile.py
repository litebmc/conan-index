"""sdbusplus构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "obmc-webui-vue"
    url = "https://github.com/openbmc/webui-vue.git"
    license = "Apache 2.0"
    description = ("webui-vue is a web-based user interface for the OpenBMC firmware stack built on Vue.js.")
    settings = []

    def export_sources(self):
        files.copy(self, "permissions", self.recipe_folder, self.export_sources_folder)

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def build(self):
        self.run("npm install --max-old-space-size=1024")
        self.run("npm run build --max-old-space-size=1024")

    def package(self):
        files.copy(self, "*", src="dist", dst=os.path.join(self.package_folder, "usr/share/www"))
        files.copy(self, "permissions", src=".", dst=os.path.join(self.package_folder))

    def package_info(self):
        pass
