import os
import hashlib

from conan import ConanFile
from conan.tools.files import get, copy
from conan.errors import ConanInvalidConfiguration
from conan.tools.gnu import Autotools, AutotoolsToolchain
from conan.tools.env import VirtualBuildEnv

required_conan_version = ">=1.53.0"


class BuildrootConan(ConanFile):
    name = "buildroot"
    description = "A tool to generate embedded Linux systems through cross-compilation."
    license = "GPL-2.0"
    homepage = "https://buildroot.org/"
    topics = ("shell", "tools")

    package_type = "application"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "defconfig": ["ANY"],
        "defsha256": ["ANY"]
    }
    default_options = {
        "defconfig": "configs/default_defconfig",
        "defsha256": "d216cb94138791cc829d774251b0bf14cfee6dab6a97d8bb0ce1c006e73c1ee6",
    }

    @property
    def _settings_build(self):
        return getattr(self, "settings_build", self.settings)

    def export_sources(self):
        copy(self, "configs/*", os.path.join(self.recipe_folder), self.export_sources_folder)

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        ve = VirtualBuildEnv(self)
        env = ve.environment()
        env.unset("STRIP")
        ve.generate()
        tc = AutotoolsToolchain(self)
        env = tc.environment()
        env.unset("CPPFLAGS")
        env.unset("CXXFLAGS")
        env.unset("CFLAGS")
        env.unset("LDFLAGS")
        env.unset("STRIP")
        tc.generate()

    def validate(self):
        defconfig = str(self.options.defconfig)
        if not defconfig.endswith("_defconfig"):
            raise ConanInvalidConfiguration(f"defconfig file ({defconfig}) not endswith _defconfig")

    def build(self):
        defconfig = str(self.options.defconfig)
        defconfig = os.path.realpath(defconfig)
        if not os.path.isfile(defconfig):
            raise ConanInvalidConfiguration(f"defconfig file ({defconfig}) not exist")

        sha256 = hashlib.sha256()
        with open(defconfig, 'rb') as f:
            while True:
                data = f.read(4096)
                if not data:
                    break
                sha256.update(data)
        real_sha256 = sha256.hexdigest()
        if real_sha256 != str(self.options.defsha256):
            raise ConanInvalidConfiguration(f"the sha256sum of defconfig file ({defconfig}) not match," \
                                            f"need: {self.options.defsha256}, get: {real_sha256}")

        dstdir = os.path.join(self.build_folder, "configs")
        dstfile = os.path.basename(defconfig)
        if os.path.join(dstdir, dstfile) != defconfig:
            copy(self, pattern=dstfile, src=os.path.dirname(defconfig), dst=dstdir)
        backup_site = os.environ.get("LB_CI_ENV_BACKUP_SOURCE_SITE")
        real_file = os.path.join(dstdir, dstfile)
        self.run(f"sed -i 's/^BR2_TARGET_ROOTFS/# BR2_TARGET_ROOTFS/g' {real_file}" )
        if backup_site:
            with open(real_file, "a+") as fp:
                fp.write("\n")
                fp.write(f"BR2_PRIMARY_SITE=\"{backup_site}\"\n")
                fp.write(f"BR2_TARGET_ROOTFS_TAR=y\n")
        autotools = Autotools(self)
        args = [
            dstfile,
            "-j4"
        ]
        autotools.make(args=args)
        autotools.make()

    def package(self):
        copy(self, "rootfs.tar", src=os.path.join(self.build_folder, "output/images"), dst=self.package_folder)

