
from conan import ConanFile
from conan.tools.files import export_conandata_patches, get
from conan.tools.gnu import Autotools, AutotoolsToolchain
from conan.tools.layout import basic_layout

required_conan_version = ">=1.53.0"


class GdbServerConan(ConanFile):
    name = "gdbserver"
    description = "The GNU Project Debugger."
    license = "GPL-2.0"
    homepage = "https://www.sourceware.org/gdb/"
    topics = ("debugger")

    package_type = "application"
    settings = "os", "arch", "compiler", "build_type"

    @property
    def _settings_build(self):
        return getattr(self, "settings_build", self.settings)

    def export_sources(self):
        export_conandata_patches(self)

    def layout(self):
        basic_layout(self, src_folder="src")

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        tc = AutotoolsToolchain(self)
        tc.configure_args.append("--without-uiout")
        tc.configure_args.append("--disable-gdbtk")
        tc.configure_args.append("--without-x")
        tc.configure_args.append("--disable-sim")
        tc.configure_args.append("--disable-binutils")
        tc.configure_args.append("--disable-install-libbfd")
        tc.configure_args.append("--disable-ld")
        tc.configure_args.append("--disable-gas")
        tc.configure_args.append("--disable-gprof")
        tc.configure_args.append("--without-included-gettext")
        tc.configure_args.append("--disable-werror")
        tc.configure_args.append("--enable-static")
        tc.configure_args.append("--disable-shared")
        tc.configure_args.append("--without-mpfr")
        tc.configure_args.append("--disable-source-highlight")
        tc.configure_args.append("--without-python")
        tc.configure_args.append("--disable-gdb")
        tc.configure_args.append("--without-curses")
        tc.configure_args.append("--without-system-zlib")
        tc.configure_args.append("--enable-gdbserver")
        tc.configure_args.append("--without-expat")
        tc.configure_args.append("--without-lzma")

        tc.generate()

    def build(self):
        autotools = Autotools(self)
        autotools.configure()
        autotools.make(target="all-gdbserver")

    def package(self):
        autotools = Autotools(self)
        autotools.install(target="install-gdbserver")
