"""phosphor-logging构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"


# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-logging"
    url = "https://github.com/openbmc/phosphor-logging.git"
    license = "Apache 2.0"
    description = ("Provides mechanisms for event and journal logging")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "libonly": [True, False],
        "openpower_pel_extension": [True, False],
        "yamldir": [None, "ANY"],
        "callout_yaml": ["ANY"],
        "error_cap": ["ANY"],
        "error_info_cap": ["ANY"],
        "phal": [True, False],
        "rsyslog_server_conf": ["ANY"],
    }
    default_options = {
        "libonly": False,
        "openpower_pel_extension": False,
        "yamldir": None,
        "callout_yaml": "",
        "error_cap": 200,
        "error_info_cap": 256,
        "phal": False,
        "rsyslog_server_conf": "/etc/rsyslog.d/server.conf",
    }

    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)
        files.copy(self, "*", src="files", dst=os.path.join(self.export_sources_folder, "files"))

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])
        files.rmdir(self, os.path.join(self.source_folder, "subprojects"))

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["datadir"] = "usr/share"
        tc.project_options["libonly"] = (True if self.options.libonly else False)
        tc.project_options["tests"] = "disabled"
        tc.project_options["openpower-pel-extension"] = ("enabled" if self.options.openpower_pel_extension else "disabled")
        if self.options.yamldir:
            tc.project_options["yamldir"] = str(self.options.yamldir)
        if str(self.options.callout_yaml):
            tc.project_options["callout_yaml"] = str(self.options.callout_yaml)
        else:
            tc.project_options["callout_yaml"] = os.path.join(self.export_sources_folder, "files", "callouts.yaml")
        tc.project_options["error_cap"] = str(self.options.error_cap)
        tc.project_options["error_info_cap"] = str(self.options.error_info_cap)
        tc.project_options["phal"] = "disabled"
        tc.project_options["rsyslog_server_conf"] = str(self.options.rsyslog_server_conf)
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        pass

    def package_info(self):
        self.cpp_info.libs = ["phosphor_logging"]
        self.cpp_info.include_dirs = ["include"]
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_target_name", "phosphor-logging::phosphor-logging")
        self.cpp_info.set_property("pkg_config_name", "phosphor-logging")
