"""phosphor-fan构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-fan"
    url = "https://github.com/openbmc/phosphor-fan.git"
    license = "Apache 2.0"
    description = ("OpenBMC PID-based Thermal Control Daemon.")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"
    options = {
        "json_config": [True, False],
        "machine_name": ["ANY"],
        "control_service": [True, False],
        "json_control": [True, False],
        "control_persist_root_path": ["ANY"],
        "fan_def_yaml_file": ["ANY"],
        "fan_zone_yaml_file": ["ANY"],
        "zone_events_yaml_file": ["ANY"],
        "zone_conditions_yaml_file": ["ANY"],
        "monitor_service": [True, False],
        "fan_monitor_yaml_file": ["ANY"],
        "num_monitor_log_entries": ["ANY"],
        "delay_host_control": ["ANY"],
        "monitor_use_host_state": [True, False],
        "presence_service": [True, False],
        "presence_config": ["ANY"],
        "num_presence_log_entries": ["ANY"],
        "sensor_monitor_service": [True, False],
        "sensor_monitor_persist_root_path": ["ANY"],
        "sensor_monitor_hard_shutdown_delay": ["ANY"],
        "sensor_monitor_soft_shutdown_delay": ["ANY"],
        "cooling_type_service": [True, False],
        "use_host_power_state": [True, False],
    }
    default_options = {
        "json_config": True,
        "machine_name": "",
        "control_service": True,
        "json_control": True,
        "control_persist_root_path": "/var/lib/phosphor-fan-presence/control",
        "fan_def_yaml_file": "example/fans.yaml",
        "fan_zone_yaml_file": "example/zones.yaml",
        "zone_events_yaml_file": "example/events.yaml",
        "zone_conditions_yaml_file": "example/zone_conditions.yaml",
        "monitor_service": True,
        "fan_monitor_yaml_file": "example/monitor.yaml",
        "num_monitor_log_entries": 75,
        "delay_host_control": 0,
        "monitor_use_host_state": False,
        "presence_service": True,
        "presence_config": "example/example.yaml",
        "num_presence_log_entries": 50,
        "sensor_monitor_service": True,
        "sensor_monitor_persist_root_path": "/var/lib/phosphor-fan-presence/sensor-monitor",
        "sensor_monitor_hard_shutdown_delay": 23000,
        "sensor_monitor_soft_shutdown_delay": 900000,
        "cooling_type_service": False,
        "use_host_power_state": False,
    }

    def export_sources(self):
        files.export_conandata_patches(self)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["json-config"] = ("enabled" if self.options.json_config else "disabled")
        tc.project_options["machine-name"] = str(self.options.machine_name)
        tc.project_options["control-service"] = ("enabled" if self.options.control_service else "disabled")
        tc.project_options["json-control"] = ("enabled" if self.options.json_control else "disabled")
        tc.project_options["control-persist-root-path"] = str(self.options.control_persist_root_path)
        tc.project_options["fan-def-yaml-file"] = str(self.options.fan_def_yaml_file)
        tc.project_options["fan-zone-yaml-file"] = str(self.options.fan_zone_yaml_file)
        tc.project_options["zone-events-yaml-file"] = str(self.options.zone_events_yaml_file)
        tc.project_options["zone-conditions-yaml-file"] = str(self.options.zone_conditions_yaml_file)
        tc.project_options["monitor-service"] = ("enabled" if self.options.monitor_service else "disabled")
        tc.project_options["fan-monitor-yaml-file"] = str(self.options.fan_monitor_yaml_file)
        tc.project_options["num-monitor-log-entries"] = int(self.options.num_monitor_log_entries)
        tc.project_options["delay-host-control"] = int(self.options.delay_host_control)
        tc.project_options["monitor-use-host-state"] = ("enabled" if self.options.monitor_use_host_state else "disabled")
        tc.project_options["presence-service"] = ("enabled" if self.options.presence_service else "disabled")
        tc.project_options["presence-config"] = str(self.options.presence_config)
        tc.project_options["num-presence-log-entries"] = int(self.options.num_presence_log_entries)
        tc.project_options["sensor-monitor-service"] = ("enabled" if self.options.sensor_monitor_service else "disabled")
        tc.project_options["sensor-monitor-persist-root-path"] = str(self.options.sensor_monitor_persist_root_path)
        tc.project_options["sensor-monitor-hard-shutdown-delay"] = int(self.options.sensor_monitor_hard_shutdown_delay)
        tc.project_options["sensor-monitor-soft-shutdown-delay"] = int(self.options.sensor_monitor_soft_shutdown_delay)
        tc.project_options["cooling-type-service"] = ("enabled" if self.options.cooling_type_service else "disabled")
        tc.project_options["use-host-power-state"] = ("enabled" if self.options.use_host_power_state else "disabled")
        if self.settings.build_type != "Debug":
            tc.project_options["optimization"] = "s"
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        pass

    def package_info(self):
        pass
