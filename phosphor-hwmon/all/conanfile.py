"""phosphor-bmc-code-mgmt构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-hwmon"
    url = "https://github.com/openbmc/phosphor-hwmon.git"
    license = "Apache 2.0"
    description = ("Exposes generic hwmon entries as DBus objects")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "busname_prefix": ["ANY"],
        "negative_errno_on_fail": [True, False],
        "sensor_root": ["ANY"],
        "update_functional_on_fail": [True, False],
        "enable_max31785_msl": [True, False],
        "override_with_devpath": ["ANY"],
    }
    default_options = {
        "busname_prefix": "xyz.openbmc_project.Hwmon",
        "negative_errno_on_fail": True,
        "sensor_root": "/xyz/openbmc_project/sensors",
        "update_functional_on_fail": True,
        "enable_max31785_msl": False,
        "override_with_devpath": "",
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def export_sources(self):
        files.export_conandata_patches(self)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["busname-prefix"] = str(self.options.busname_prefix)
        tc.project_options["negative-errno-on-fail"] = (True if self.options.negative_errno_on_fail else False)
        tc.project_options["sensor-root"] = str(self.options.sensor_root)
        tc.project_options["tests"] = "disabled"
        tc.project_options["update-functional-on-fail"] = (True if self.options.update_functional_on_fail else False)
        tc.project_options["enable-max31785-msl"] = (True if self.options.enable_max31785_msl else False)
        tc.project_options["override-with-devpath"] = str(self.options.override_with_devpath).split(",")
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", src=os.path.join(self.build_folder, "rootfs"), dst=self.package_folder)
        pass

    def package_info(self):
        pass
