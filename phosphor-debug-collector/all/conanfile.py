"""Collects debug data from the BMC for extraction.构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-debug-collector"
    url = "https://github.com/openbmc/phosphor-debug-collector.git"
    license = "Apache 2.0"
    description = ("Collects debug data from the BMC for extraction.")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"

    options = {
        "jffs_workaround": [True, False],
        "DUMP_BUSNAME": ["ANY"],
        "DUMP_OBJPATH": ["ANY"],
        "BMC_DUMP_OBJPATH": ["ANY"],
        "CORE_FILE_DIR": ["ANY"],
        "BMC_DUMP_OBJ_ENTRY": ["ANY"],
        "BMC_DUMP_PATH": ["ANY"],
        "SYSTEMD_PSTORE_PATH": ["ANY"],
        "BMC_DUMP_MAX_SIZE": ["ANY"],
        "BMC_DUMP_MIN_SPACE_REQD": ["ANY"],
        "BMC_DUMP_TOTAL_SIZE": ["ANY"],
        "ELOG_ID_PERSIST_PATH": ["ANY"],
        "CLASS_VERSION": ["ANY"],
        "ERROR_MAP_YAML": ["ANY"],
        "host_transport": ["ANY"],
        "openpower_dumps_extension": [True, False],
        "dump_rotate_config": [True, False],
        "FAULTLOG_DUMP_PATH": ["ANY"],
        "FAULTLOG_DUMP_OBJPATH": ["ANY"],
        "FAULTLOG_DUMP_OBJ_ENTRY": ["ANY"],
        "SYSTEM_DUMP_OBJPATH": ["ANY"],
        "SYSTEM_DUMP_OBJ_ENTRY": ["ANY"],
        "RESOURCE_DUMP_OBJPATH": ["ANY"],
        "RESOURCE_DUMP_OBJ_ENTRY": ["ANY"],
    }
    default_options = {
        "jffs_workaround": True,
        "DUMP_BUSNAME": "xyz.openbmc_project.Dump.Manager",
        "DUMP_OBJPATH": "/xyz/openbmc_project/dump",
        "BMC_DUMP_OBJPATH": "/xyz/openbmc_project/dump/bmc",
        "CORE_FILE_DIR": "/var/lib/systemd/coredump",
        "BMC_DUMP_OBJ_ENTRY": "/xyz/openbmc_project/dump/bmc/entry",
        "BMC_DUMP_PATH": "/var/lib/phosphor-debug-collector/dumps/",
        "SYSTEMD_PSTORE_PATH": "/var/lib/systemd/pstore/",
        "BMC_DUMP_MAX_SIZE": 200,
        "BMC_DUMP_MIN_SPACE_REQD": 20,
        "BMC_DUMP_TOTAL_SIZE": 1024,
        "ELOG_ID_PERSIST_PATH": "/var/lib/phosphor-debug-collector/elogid",
        "CLASS_VERSION": 1,
        "ERROR_MAP_YAML": "example_errors_watch.yaml",
        "host_transport": "default",
        "openpower_dumps_extension": False,
        "dump_rotate_config": False,
        "FAULTLOG_DUMP_PATH": "/var/lib/phosphor-debug-collector/faultlogs/",
        "FAULTLOG_DUMP_OBJPATH": "/xyz/openbmc_project/dump/faultlog",
        "FAULTLOG_DUMP_OBJ_ENTRY": "/xyz/openbmc_project/dump/faultlog/entry",
        "SYSTEM_DUMP_OBJPATH": "/xyz/openbmc_project/dump/system",
        "SYSTEM_DUMP_OBJ_ENTRY": "/xyz/openbmc_project/dump/system/entry",
        "RESOURCE_DUMP_OBJPATH": "/xyz/openbmc_project/dump/resource",
        "RESOURCE_DUMP_OBJ_ENTRY": "/xyz/openbmc_project/dump/resource/entry",
    }

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["jffs-workaround"] = ("enabled" if self.options.jffs_workaround else "disabled")
        tc.project_options["DUMP_BUSNAME"] = str(self.options.DUMP_BUSNAME)
        tc.project_options["DUMP_OBJPATH"] = str(self.options.DUMP_OBJPATH)
        tc.project_options["BMC_DUMP_OBJPATH"] = str(self.options.BMC_DUMP_OBJPATH)
        tc.project_options["CORE_FILE_DIR"] = str(self.options.CORE_FILE_DIR)
        tc.project_options["BMC_DUMP_OBJ_ENTRY"] = str(self.options.BMC_DUMP_OBJ_ENTRY)
        tc.project_options["BMC_DUMP_PATH"] = str(self.options.BMC_DUMP_PATH)
        tc.project_options["SYSTEMD_PSTORE_PATH"] = str(self.options.SYSTEMD_PSTORE_PATH)
        tc.project_options["BMC_DUMP_MAX_SIZE"] = int(self.options.BMC_DUMP_MAX_SIZE)
        tc.project_options["BMC_DUMP_MIN_SPACE_REQD"] = int(self.options.BMC_DUMP_MIN_SPACE_REQD)
        tc.project_options["BMC_DUMP_TOTAL_SIZE"] = int(self.options.BMC_DUMP_TOTAL_SIZE)
        tc.project_options["ELOG_ID_PERSIST_PATH"] = str(self.options.ELOG_ID_PERSIST_PATH)
        tc.project_options["CLASS_VERSION"] = int(self.options.CLASS_VERSION)
        tc.project_options["ERROR_MAP_YAML"] = str(self.options.ERROR_MAP_YAML)
        tc.project_options["host-transport"] = str(self.options.host_transport)
        tc.project_options["openpower-dumps-extension"] = ("enabled" if self.options.openpower_dumps_extension else "disabled")
        tc.project_options["dump_rotate_config"] = ("enabled" if self.options.dump_rotate_config else "disabled")
        tc.project_options["FAULTLOG_DUMP_PATH"] = str(self.options.FAULTLOG_DUMP_PATH)
        tc.project_options["FAULTLOG_DUMP_OBJPATH"] = str(self.options.FAULTLOG_DUMP_OBJPATH)
        tc.project_options["FAULTLOG_DUMP_OBJ_ENTRY"] = str(self.options.FAULTLOG_DUMP_OBJ_ENTRY)
        tc.project_options["SYSTEM_DUMP_OBJPATH"] = str(self.options.SYSTEM_DUMP_OBJPATH)
        tc.project_options["SYSTEM_DUMP_OBJ_ENTRY"] = str(self.options.SYSTEM_DUMP_OBJ_ENTRY)
        tc.project_options["RESOURCE_DUMP_OBJPATH"] = str(self.options.RESOURCE_DUMP_OBJPATH)
        tc.project_options["RESOURCE_DUMP_OBJ_ENTRY"] = str(self.options.RESOURCE_DUMP_OBJ_ENTRY)
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        pass

    def package_info(self):
        pass
