"""phosphor-bmc-code-mgmt构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-bmc-code-mgmt"
    url = "https://github.com/openbmc/phosphor-bmc-code-mgmt.git"
    license = "Apache 2.0"
    description = ("Provides a set of system software management applications")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "bmc_layout": ["static", "ubi", "mmc"],
        "host_bios_upgrade": [True, False],
        "sync_bmc_files": [True, False],
        "verify_signature": [True, False],
        "usb_code_update": [True, False],
        "software_update_dbus_interface": [True, False],
        "side_switch_on_boot": [True, False],
        "active_bmc_max_allowed": ["ANY"],
        "hash_file_name": ["ANY"],
        "img_upload_dir": ["ANY"],
        "manifest_file_name": ["ANY"],
        "media_dir": ["ANY"],
        "optional_images": ["ANY"],
        "publickey_file_name": ["ANY"],
        "signature_file_ext": ["ANY"],
        "signed_image_conf_path": ["ANY"],
        "sync_list_dir_path": ["ANY"],
        "sync_list_file_name": ["ANY"],
        "bmc_msl": ["ANY"],
        "regex_bmc_msl": ["ANY"],
        "bios_object_path": ["ANY"],
        "bmc_static_dual_image": [True, False],
        "alt_rofs_dir": ["ANY"],
        "alt_rwfs_dir": ["ANY"],
    }
    default_options = {
        "bmc_layout": "static",
        "host_bios_upgrade": True,
        "sync_bmc_files": True,
        "verify_signature": True,
        "usb_code_update": True,
        "software_update_dbus_interface": True,
        "side_switch_on_boot": True,
        "active_bmc_max_allowed": 1,
        "hash_file_name": "hashfunc",
        "img_upload_dir": "/tmp/images",
        "manifest_file_name": "MANIFEST",
        "media_dir": "/run/media",
        "optional_images": "",
        "publickey_file_name": "publickey",
        "signature_file_ext": ".sig",
        "signed_image_conf_path": "/etc/activationdata/",
        "sync_list_dir_path": "/etc/",
        "sync_list_file_name": "synclist",
        "bmc_msl": ",description:",
        "regex_bmc_msl": ",description:",
        "bios_object_path": "/xyz/openbmc_project/software/bios_active",
        "bmc_static_dual_image": True,
        "alt_rofs_dir": "/run/media/rofs-alt",
        "alt_rwfs_dir": "/run/media/rwfs-alt/cow",
    }

    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["bmc-layout"] = str(self.options.bmc_layout)
        tc.project_options["host-bios-upgrade"] = ("enabled" if self.options.host_bios_upgrade else "disabled")
        tc.project_options["sync-bmc-files"] = ("enabled" if self.options.sync_bmc_files else "disabled")
        tc.project_options["tests"] = "disabled"
        tc.project_options["verify-signature"] = ("enabled" if self.options.verify_signature else "disabled")
        tc.project_options["usb-code-update"] = ("enabled" if self.options.usb_code_update else "disabled")
        tc.project_options["software-update-dbus-interface"] = ("enabled" if self.options.software_update_dbus_interface else "disabled")
        tc.project_options["side-switch-on-boot"] = ("enabled" if self.options.side_switch_on_boot else "disabled")
        tc.project_options["active-bmc-max-allowed"] = int(self.options.active_bmc_max_allowed)
        tc.project_options["hash-file-name"] = str(self.options.hash_file_name)
        tc.project_options["img-upload-dir"] = str(self.options.img_upload_dir)
        tc.project_options["manifest-file-name"] = str(self.options.manifest_file_name)
        tc.project_options["media-dir"] = str(self.options.media_dir)
        tc.project_options["optional-images"] = str(self.options.optional_images).split(",")
        tc.project_options["publickey-file-name"] = str(self.options.publickey_file_name)
        tc.project_options["signature-file-ext"] = str(self.options.signature_file_ext)
        tc.project_options["signed-image-conf-path"] = str(self.options.signed_image_conf_path)
        tc.project_options["sync-list-dir-path"] = str(self.options.sync_list_dir_path)
        tc.project_options["sync-list-file-name"] = str(self.options.sync_list_file_name)
        tc.project_options["bmc-msl"] = str(self.options.bmc_msl)
        tc.project_options["regex-bmc-msl"] = str(self.options.regex_bmc_msl)
        tc.project_options["bios-object-path"] = str(self.options.bios_object_path)
        tc.project_options["bmc-static-dual-image"] = ("enabled" if self.options.bmc_static_dual_image else "disabled")
        tc.project_options["alt-rofs-dir"] = str(self.options.alt_rofs_dir)
        tc.project_options["alt-rwfs-dir"] = str(self.options.alt_rwfs_dir)
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        files.copy(self, "*", src=os.path.join(self.build_folder, "rootfs"), dst=self.package_folder)
        pass

    def package_info(self):
        pass
