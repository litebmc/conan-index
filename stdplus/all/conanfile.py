from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class StdPlusConan(ConanFile):
    name = "stdplus"
    url = "https://github.com/openbmc/sdeventplus.git"
    license = "Apache 2.0"
    description = ("stdplus is a c++ project containing commonly used classes and functions for the Linux platform.")
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "examples": [True, False],
        "dl": [True, False],
        "fd": [True, False],
        "io_uring": [True, False],
        "gtest": [True, False]
    }
    default_options = {
        "dl": True,
        "fd": True,
        "io_uring": True,
        "gtest": False,
        "examples": True,
    }
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["examples"] = (True if self.options.examples else False)
        tc.project_options["tests"] = "disabled"
        tc.project_options["dl"] = ("enabled" if self.options.dl else "disabled")
        tc.project_options["fd"] = ("enabled" if self.options.fd else "disabled")
        tc.project_options["io_uring"] = ("enabled" if self.options.io_uring else "disabled")
        tc.project_options["gtest"] = ("enabled" if self.options.gtest else "disabled")
        tc.generate()


    def build(self):
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        pass

    def package_info(self):
        self.cpp_info.libs = ["stdplus", "stdplus-dl", "stdplus-io_uring"]
        self.cpp_info.include_dirs = ["include"]
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_target_name", "stdplus::stdplus")
        self.cpp_info.set_property("pkg_config_name", "stdplus")
