"""phosphor-time-manager构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-time-manager"
    url = "https://github.com/openbmc/phosphor-time-manager.git"
    license = "Apache 2.0"
    description = ("Time manager service")
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "default_time_mode": ["Mode::Manual", "Mode::NTP"],
        # "default_time_sync_object_path": ["ANY"],
    }
    default_options = {
        "default_time_mode": "Mode::Manual",
        # "default_time_sync_object_path": "/xyz/openbmc_project/time/sync_method",
    }

    requires = [
        "phosphor-dbus-interfaces/[>=2.14.0]",
        "phosphor-logging/[>=2.14.0]"
    ]

    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def generate(self):
        tc = MesonToolchain(self)
        tc.cpp_args.append("-Wno-unused-parameter")
        tc.cpp_args.append("-Wno-error=unused-parameter")
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["default_time_mode"] = str(self.options.default_time_mode)
        # tc.project_options["default_time_sync_object_path"] = str(self.options.default_time_sync_object_path)
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        files.copy(self, "*", src=os.path.join(self.build_folder, "rootfs"), dst=self.package_folder)
        pass

    def package_info(self):
        pass
