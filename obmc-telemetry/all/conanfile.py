import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "obmc-telemetry"
    url = "https://github.com/openbmc/telemetry.git"
    license = "Apache 2.0"
    description = ("This component implements middleware for sensors and metrics aggregation..")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"
    options = {
        "buildtest": [True, False],
        "max_reports": ["ANY"],
        "max_reading_parameters": ["ANY"],
        "min_interval": ["ANY"],
        "max_triggers": ["ANY"],
        "max_dbus_path_length": ["ANY"],
        "max_append_limit": ["ANY"],
        "max_id_name_length": ["ANY"],
        "max_prefix_length": ["ANY"],
        "service_wants": ["ANY"],
        "service_requires": ["ANY"],
        "service_before": ["ANY"],
        "service_after": ["ANY"],
    }
    default_options = {
        "buildtest": False,
        "max_reports": 10,
        "max_reading_parameters": 200,
        "min_interval": 1000,
        "max_triggers": 10,
        "max_dbus_path_length": 4095,
        "max_append_limit": 256,
        "max_id_name_length": 256,
        "max_prefix_length": 256,
        "service_wants": "",
        "service_requires": "",
        "service_before": "",
        "service_after": "",
    }
    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["buildtest"] = (True if self.options.buildtest else False)
        tc.project_options["max-reports"] = int(self.options.max_reports)
        tc.project_options["max-reading-parameters"] = int(self.options.max_reading_parameters)
        tc.project_options["min-interval"] = int(self.options.min_interval)
        tc.project_options["max-triggers"] = int(self.options.max_triggers)
        tc.project_options["max-dbus-path-length"] = int(self.options.max_dbus_path_length)
        tc.project_options["max-append-limit"] = int(self.options.max_append_limit)
        tc.project_options["max-id-name-length"] = int(self.options.max_id_name_length)
        tc.project_options["max-prefix-length"] = int(self.options.max_prefix_length)
        tc.project_options["service-wants"] = str(self.options.service_wants).split(",")
        tc.project_options["service-requires"] = str(self.options.service_requires).split(",")
        tc.project_options["service-before"] = str(self.options.service_before).split(",")
        tc.project_options["service-after"] = str(self.options.service_after).split(",")
        tc.generate()

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        pass

    def package_info(self):
        pass
