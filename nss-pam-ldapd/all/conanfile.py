import os
from conan import ConanFile
from conan.tools.layout import basic_layout
from conan.tools.files import get
from conan.tools.gnu import AutotoolsToolchain, Autotools


class NssPamLdapdConan(ConanFile):
    name = "nss-pam-ldapd"
    url = "https://arthurdejong.org/git/nss-pam-ldapd"
    homepage = "https://arthurdejong.org/git/nss-pam-ldapd"
    license = "LGPL-2.1"
    description = ("NSS and PAM libraries for name lookups and authentication using LDAP")
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    settings = "os", "arch", "compiler", "build_type"

    requires = [
        "linux-pam/[>=1.5.3]",
        "openldap/2.6.7",
        "autoconf/2.71",
        "automake/1.16.5",
    ]
    options = {
        "enable_debug": [True, False],
        "enable_warnings": [True, False],
        "disable_maintainer_mode": [True, False],
        "disable_nss": [True, False],
        "disable_pam": [True, False],
        "disable_utils": [True, False],
        "disable_nslcd": [True, False],
        "enable_pynslcd": [True, False],
        "disable_sasl": [True, False],
        # "disable_kerberos": [True, False],
        "disable_configfile_checking": [True, False],
    }
    default_options = {
        "enable_debug": False,
        "enable_warnings": False,
        "disable_maintainer_mode": False,
        "disable_nss": False,
        "disable_pam": False,
        "disable_utils": True,
        "disable_nslcd": False,
        "enable_pynslcd": False,
        "disable_sasl": False,
        # "disable_kerberos": True,
        "disable_configfile_checking": False,
    }
    generators = "AutotoolsDeps"

    exports_sources = "nslcd.service"

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/sbin"]

    # def configure(self):
    #     if not self.options.disable_kerberos:
    #         self.options["openssl"].shared = True

    def requirements(self):
        if not self.options.disable_sasl:
            self.requires("cyrus-sasl/[>=2.1.27]")
        # if not self.options.disable_kerberos:
        #     self.requires("krb5/[>=1.21.2]")

    def build_requirements(self):
        self.tool_requires("autoconf/2.71")
        self.tool_requires("automake/1.16.5")

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        at = AutotoolsToolchain(self)
        at.generate()

    def build(self):
        args = []
        if self.options.enable_debug:
            args.append("--enable-debug")
        if self.options.enable_warnings:
            args.append("--enable-warnings")
        if self.options.disable_maintainer_mode:
            args.append("---maintainer-mode"),
        if self.options.disable_nss:
            args.append("--disable-nss")
        if self.options.disable_pam:
            args.append("--disable-pam")
        if self.options.disable_utils:
            args.append("--disable-utils")
        if self.options.disable_nslcd:
            args.append("--disable-nslcd")
        if self.options.enable_pynslcd:
            args.append("--enable-pynslcd")
        if self.options.disable_sasl:
            args.append("--disable-sasl")
        if self.options.disable_configfile_checking:
            args.append("---configfile-checking")
        # 编译krb5失败，禁用
        args.append("--disable-kerberos")
        autotools = Autotools(self)
        autotools.configure(args=args)
        autotools.make()
        autotools.install()

    def package(self):
        system_unit_dir = os.path.join(self.package_folder, "lib", "systemd", "system")
        self.run(f"mkdir -p {self.package_folder}/etc/systemd/system/multi-user.target.wants/")
        self.run(f"mkdir -p {system_unit_dir}")
        self.run(f"cp {self.source_folder}/nslcd.service {system_unit_dir}")
        self.run(f"cp {self.source_folder}/nslcd.conf {self.package_folder}/etc/nslcd.conf.default")
        self.run(f"ln -s /lib/systemd/system/nslcd.service {self.package_folder}/etc/systemd/system/multi-user.target.wants/nslcd.service")

    def package_info(self):
        pass