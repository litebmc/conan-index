"""phosphor-centificate-manager构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-certificate-manager"
    url = "https://github.com/openbmc/phosphor-certificate-manager.git"
    license = "Apache 2.0"
    description = ("Certificate management allows to replace the existing certificate and private key file with another (possibly CA signed) Certificate key file. ")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"
    options = {
        "authority_limit": ["ANY"],
        "ca_cert_extension": [True, False],
        "config_bmcweb": [True, False],
        "config_authority": [True, False],
        "authorities_list_name": ["ANY"],
        "allow_expired": [True, False],
    }
    default_options = {
        "authority_limit": 10,
        "ca_cert_extension": True,
        "config_bmcweb": True,
        "config_authority": True,
        "authorities_list_name": "trust_bundle",
        "allow_expired": True,
    }
    def generate(self):
        tc = MesonToolchain(self)
        tc.cpp_args.append("-Wno-unused-value")
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["authority-limit"] = int(self.options.authority_limit)
        tc.project_options["ca-cert-extension"] = ("enabled" if self.options.ca_cert_extension else "disabled")
        tc.project_options["config-bmcweb"] = ("enabled" if self.options.config_bmcweb else "disabled")
        tc.project_options["config-authority"] = ("enabled" if self.options.config_authority else "disabled")
        tc.project_options["authorities-list-name"] = str(self.options.authorities_list_name)
        tc.project_options["allow-expired"] = ("enabled" if self.options.allow_expired else "disabled")
        tc.generate()

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)
    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        pass

    def package_info(self):
        pass
