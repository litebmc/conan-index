from conans import ConanFile, tools, AutoToolsBuildEnvironment
import os

class NtpConan(ConanFile):
    name = "ntp"
    url = "http://www.ntp.org/index.html"
    homepage = "http://www.ntp.org"
    license = "NTP"
    description = ("Network Time Protocol Version 4 (NTP)")
    settings = "os", "arch", "compiler", "build_type"
    generators = "cmake"

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    @property
    def _build_subfolder(self):
        return "build_subfolder"

    def source(self):
        tools.get(**self.conan_data["sources"][self.version],
                  destination=self._source_subfolder, strip_root=True)

    def build(self):
        os.chdir(self._source_subfolder)
        autotools = AutoToolsBuildEnvironment(self)
        if self.settings.arch != "x86_64":
            chost = os.environ["CHOST"]
            autotools.configure(args=[f"--host={chost}", "--with-yielding-select=yes"], build=False)
        else:
            autotools.configure(build=False)
        autotools.make()
        autotools.install()

    def package(self):
        pass

    def package_info(self):
        pass
