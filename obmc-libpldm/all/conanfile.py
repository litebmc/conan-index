"""obmc-libpldm构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "obmc-libpldm"
    url = "https://github.com/openbmc/libpldm.git"
    license = "Apache 2.0"
    description = ("This is a library which deals with the encoding and decoding of PLDM messages.")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"
    options = {
        "abi": ["ANY"],
        "oem_ibm": [True, False],
        "abi_compliance_check": [True, False],
        "oem_meta": [True, False],
    }
    default_options = {
        "abi": "deprecated,stable,testing",
        "oem_ibm": True,
        "abi_compliance_check": True,
        "oem_meta": True,
    }

    def export_sources(self):
        files.export_conandata_patches(self)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["abi"] = str(self.options.abi).split(",")
        tc.project_options["tests"] = "disabled"
        tc.project_options["oem-ibm"] = ("enabled" if self.options.oem_ibm else "disabled")
        tc.project_options["abi-compliance-check"] = ("enabled" if self.options.abi_compliance_check else "disabled")
        tc.project_options["oem-meta"] = ("enabled" if self.options.oem_meta else "disabled")
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        pass

    def package_info(self):
        self.cpp_info.libs = ["libpldm"]
        self.cpp_info.include_dirs = ["include"]
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_target_name", "libpldm::libpldm")
        self.cpp_info.set_property("pkg_config_name", "libpldm")
