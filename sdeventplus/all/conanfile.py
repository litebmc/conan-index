"""sdeventplus构建脚本"""
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git

required_conan_version = ">=2.0.5"


# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class SdeventPlusConan(ConanFile):
    name = "sdeventplus"
    url = "https://github.com/openbmc/sdeventplus.git"
    license = "Apache 2.0"
    description = ("sdeventplus is a c++ wrapper around the systemd sd_event apis meant to provide c++ ergonomics to their usage.")
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "examples": [True, False],
        "tests": [True, False]
    }
    default_options = {
        "examples": False,
        "tests": False
    }
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])
        # 添加function2依赖
        self.run("sed -i \"/^sdeventplus_deps/a\  dependency\('function2'),\" src/meson.build")

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["examples"] = (True if self.options.examples else False)
        tc.project_options["tests"] = "disabled"
        tc.generate()

    def build(self):
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        pass

    def package_info(self):
        self.cpp_info.libs = ["sdeventplus"]
        self.cpp_info.include_dirs = ["include"]
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_target_name", "sdeventplus::sdeventplus")
        self.cpp_info.set_property("pkg_config_name", "sdeventplus")
