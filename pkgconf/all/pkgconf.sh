#!/bin/bash

folder=`dirname $0`
$folder/pkgconf.ori "$@" >> /dev/null 2>&1
if [ $? -ne 0 ]; then
    export PKG_CONFIG_LIBDIR=${HOST_PKG_CONFIG_PATH}
    if [[ $1 != "--variable="* ]]; then
        export PKG_CONFIG_SYSROOT_DIR=${CONAN_CMAKE_SYSROOT}
    else
        export PKG_CONFIG_SYSROOT_DIR=
    fi
else
    export PKG_CONFIG_SYSROOT_DIR=/
fi
exec $folder/pkgconf.ori "$@"
