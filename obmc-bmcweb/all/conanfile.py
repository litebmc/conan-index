"""openbmc-bmcweb构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"


# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "obmc-bmcweb"
    url = "https://github.com/openbmc/bmcweb.git"
    license = "Apache 2.0"
    description = ("\"do everything\" embedded webserver for OpenBMC.")
    settings = "os", "arch", "compiler", "build_type"
    languages = "C++"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "kvm": [True, False],
        "vm_websocket": [True, False],
        "rest": [True, False],
        "redfish": [True, False],
        "host_serial_socket": [True, False],
        "static_hosting": [True, False],
        "redfish_bmc_journal": [True, False],
        "redfish_cpu_log": [True, False],
        "redfish_dump_log": [True, False],
        "redfish_dbus_log": [True, False],
        "redfish_host_logger": [True, False],
        "redfish_provisioning_feature": [True, False],
        "redfish_manager_uri_name": ["ANY"],
        "redfish_system_uri_name": ["ANY"],
        "bmcweb_logging": ["disabled", "enabled", "debug", "info", "warning", "error", "critical"],
        "basic_auth": [True, False],
        "session_auth": [True, False],
        "xtoken_auth": [True, False],
        "cookie_auth": [True, False],
        "mutual_tls_auth": [True, False],
        "mutual_tls_common_name_parsing": ["username", "meta"],
        "ibm_management_console": [True, False],
        "google_api": [True, False],
        "http_body_limit": ["ANY"],
        "redfish_new_powersubsystem_thermalsubsystem": [True, False],
        "redfish_allow_deprecated_power_thermal": [True, False],
        "redfish_oem_manager_fan_data": [True, False],
        "redfish_updateservice_use_dbus": [True, False],
        "https_port": ["ANY"],
        "dns_resolver": ["systemd-dbus", "asio"],
        "redfish_aggregation": [True, False],
        "experimental_redfish_multi_computer_system": [True, False],
        "experimental_http2": [True, False],
        "insecure_disable_csrf": [True, False],
        "insecure_disable_ssl": [True, False],
        "insecure_disable_auth": [True, False],
        "insecure_tftp_update": [True, False],
        "insecure_ignore_content_type": [True, False],
        "insecure_push_style_notification": [True, False],
        "insecure_enable_redfish_query": [True, False],
    }
    default_options = {
        "kvm": False,
        "vm_websocket": False,
        "rest": False,
        "redfish": True,
        "host_serial_socket": True,
        "static_hosting": True,
        "redfish_bmc_journal": False,
        "redfish_cpu_log": False,
        "redfish_dump_log": True,
        "redfish_dbus_log": True,
        "redfish_host_logger": True,
        "redfish_provisioning_feature": False,
        "redfish_manager_uri_name": "bmc",
        "redfish_system_uri_name": "system",
        "bmcweb_logging": "error",
        "basic_auth": True,
        "session_auth": True,
        "xtoken_auth": True,
        "cookie_auth": True,
        "mutual_tls_auth": True,
        "mutual_tls_common_name_parsing": "username",
        "ibm_management_console": True,
        "google_api": False,
        "http_body_limit": 400,
        "redfish_new_powersubsystem_thermalsubsystem": True,
        "redfish_allow_deprecated_power_thermal": False,
        "redfish_oem_manager_fan_data": False,
        "redfish_updateservice_use_dbus": False,
        "https_port": 443,
        "dns_resolver": "systemd-dbus",
        "redfish_aggregation": False,
        "experimental_redfish_multi_computer_system": False,
        "experimental_http2": False,
        "insecure_disable_csrf": False,
        "insecure_disable_ssl": False,
        "insecure_disable_auth": False,
        "insecure_tftp_update": False,
        "insecure_ignore_content_type": False,
        "insecure_push_style_notification": False,
        "insecure_enable_redfish_query": True
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["bin"]

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def generate(self):
        tc = MesonToolchain(self)
        tc.cpp_args.append("-Wno--Wnull-dereference")
        tc.cpp_args.append("-Wno-error=null-dereference")
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["kvm"] = ("enabled" if self.options.kvm else "disabled")
        tc.project_options["tests"] = "disabled"
        tc.project_options["vm-websocket"] = ("enabled" if self.options.vm_websocket else "disabled")
        tc.project_options["rest"] = ("enabled" if self.options.rest else "disabled")
        tc.project_options["redfish"] = ("enabled" if self.options.redfish else "disabled")
        tc.project_options["host-serial-socket"] = ("enabled" if self.options.host_serial_socket else "disabled")
        tc.project_options["static-hosting"] = ("enabled" if self.options.static_hosting else "disabled")
        tc.project_options["redfish-bmc-journal"] = ("enabled" if self.options.redfish_bmc_journal else "disabled")
        tc.project_options["redfish-cpu-log"] = ("enabled" if self.options.redfish_cpu_log else "disabled")
        tc.project_options["redfish-dump-log"] = ("enabled" if self.options.redfish_dump_log else "disabled")
        tc.project_options["redfish-dbus-log"] = ("enabled" if self.options.redfish_dbus_log else "disabled")
        tc.project_options["redfish-host-logger"] = ("enabled" if self.options.redfish_host_logger else "disabled")
        tc.project_options["redfish-provisioning-feature"] = ("enabled" if self.options.redfish_provisioning_feature else "disabled")
        tc.project_options["redfish-manager-uri-name"] = str(self.options.redfish_manager_uri_name)
        tc.project_options["redfish-system-uri-name"] = str(self.options.redfish_system_uri_name)
        tc.project_options["bmcweb-logging"] = str(self.options.bmcweb_logging)
        tc.project_options["basic-auth"] = ("enabled" if self.options.basic_auth else "disabled")
        tc.project_options["session-auth"] = ("enabled" if self.options.session_auth else "disabled")
        tc.project_options["xtoken-auth"] = ("enabled" if self.options.xtoken_auth else "disabled")
        tc.project_options["cookie-auth"] = ("enabled" if self.options.cookie_auth else "disabled")
        tc.project_options["mutual-tls-auth"] = ("enabled" if self.options.mutual_tls_auth else "disabled")
        tc.project_options["mutual-tls-common-name-parsing"] = str(self.options.mutual_tls_common_name_parsing)
        tc.project_options["ibm-management-console"] = ("enabled" if self.options.ibm_management_console else "disabled")
        tc.project_options["google-api"] = ("enabled" if self.options.google_api else "disabled")
        tc.project_options["http-body-limit"] = int(self.options.http_body_limit)
        tc.project_options["redfish-new-powersubsystem-thermalsubsystem"] = ("enabled" if self.options.redfish_new_powersubsystem_thermalsubsystem else "disabled")
        tc.project_options["redfish-allow-deprecated-power-thermal"] = ("enabled" if self.options.redfish_allow_deprecated_power_thermal else "disabled")
        tc.project_options["redfish-oem-manager-fan-data"] = ("enabled" if self.options.redfish_oem_manager_fan_data else "disabled")
        tc.project_options["redfish-updateservice-use-dbus"] = ("enabled" if self.options.redfish_updateservice_use_dbus else "disabled")
        tc.project_options["https_port"] = int(self.options.https_port)
        tc.project_options["dns-resolver"] = str(self.options.dns_resolver)
        tc.project_options["redfish-aggregation"] = ("enabled" if self.options.redfish_aggregation else "disabled")
        tc.project_options["experimental-redfish-multi-computer-system"] = ("enabled" if self.options.experimental_redfish_multi_computer_system else "disabled")
        tc.project_options["experimental-http2"] = ("enabled" if self.options.experimental_http2 else "disabled")
        tc.project_options["insecure-disable-csrf"] = ("enabled" if self.options.insecure_disable_csrf else "disabled")
        tc.project_options["insecure-disable-ssl"] = ("enabled" if self.options.insecure_disable_ssl else "disabled")
        tc.project_options["insecure-disable-auth"] = ("enabled" if self.options.insecure_disable_auth else "disabled")
        tc.project_options["insecure-tftp-update"] = ("enabled" if self.options.insecure_tftp_update else "disabled")
        tc.project_options["insecure-ignore-content-type"] = ("enabled" if self.options.insecure_ignore_content_type else "disabled")
        tc.project_options["insecure-push-style-notification"] = ("enabled" if self.options.insecure_push_style_notification else "disabled")
        tc.project_options["insecure-enable-redfish-query"] = ("enabled" if self.options.insecure_enable_redfish_query else "disabled")
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        self.run("mkdir -p " + os.path.join(self.package_folder, "home/bmcweb"))
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)

    def package_info(self):
        pass
