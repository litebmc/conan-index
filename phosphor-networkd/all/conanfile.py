"""构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-networkd"
    url = "https://github.com/openbmc/phosphor-networkd.git"
    license = "Apache 2.0"
    description = ("phosphor network manager.")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"
    options = {
        "uboot_env": [True, False],
        "default_link_local_autoconf": [True, False],
        "default_ipv6_accept_ra": [True, False],
        "sync_mac": [True, False],
        "hyp_nw_config": [True, False],
        "persist_mac": [True, False],
        "force_sync_mac": [True, False],
    }
    default_options = {
        "uboot_env": True,
        "default_link_local_autoconf": True,
        "default_ipv6_accept_ra": True,
        "sync_mac": True,
        "hyp_nw_config": True,
        "persist_mac": True,
        "force_sync_mac": True,
    }
    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["uboot-env"] = (True if self.options.uboot_env else False)
        tc.project_options["default-link-local-autoconf"] = (True if self.options.default_link_local_autoconf else False)
        tc.project_options["default-ipv6-accept-ra"] = (True if self.options.default_ipv6_accept_ra else False)
        tc.project_options["sync-mac"] = (True if self.options.sync_mac else False)
        tc.project_options["hyp-nw-config"] = (True if self.options.hyp_nw_config else False)
        tc.project_options["persist-mac"] = (True if self.options.persist_mac else False)
        tc.project_options["force-sync-mac"] = (True if self.options.force_sync_mac else False)
        tc.generate()

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        pass

    def package_info(self):
        pass
