"""sdbusplus构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "obmc-console"
    url = "https://github.com/openbmc/obmc-console.git"
    license = "Apache 2.0"
    description = ("OpenBMC host console infrastructure")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "udev": [True, False],
        "concurrent_servers": [True, False],
        "ssh": [True, False],
    }
    default_options = {
        "udev": True,
        "concurrent_servers": False,
        "ssh": True,
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"

    def export_sources(self):
        files.export_conandata_patches(self)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = False
        tc.project_options["udev"] = ("enabled" if self.options.udev else "disabled")
        tc.project_options["concurrent-servers"] = (True if self.options.concurrent_servers else False)
        tc.project_options["ssh"] = ("enabled" if self.options.ssh else "disabled")
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        pass

    def package_info(self):
        pass
