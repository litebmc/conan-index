import os
from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain, cmake_layout
from conan.tools.files import get, load, save

required_conan_version = ">=1.53.0"


class IniParserConan(ConanFile):
    name = "iniparser"
    description = "This modules offers parsing of ini files from C."
    url = "https://gitlab.com/iniparser/iniparser.git"
    homepage = "https://gitlab.com/iniparser/iniparser.git"
    topics = ("tools", "library")
    license = "MIT"
    settings = "os", "arch", "compiler", "build_type"
    package_type = "library"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
    }

    _cmake = None

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            del self.options.fPIC

    def layout(self):
        cmake_layout(self, src_folder=".")

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["BUILD_EXAMPLES"] = False
        tc.variables["BUILD_DOCS"] = False
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_file_name", "iniparser")
        self.cpp_info.set_property("cmake_target_name", "iniparser::iniparser")
        self.cpp_info.set_property("pkg_config_name", "iniparser")

        self.cpp_info.components["iniparser"].libs = ["iniparser"]
        self.cpp_info.components["iniparser"].includedirs = ["include"]

        self.cpp_info.filenames["cmake_find_package"] = "iniparser"
        self.cpp_info.filenames["cmake_find_package_multi"] = "iniparser"
        self.cpp_info.names["cmake_find_package"] = "iniparser"
        self.cpp_info.names["cmake_find_package_multi"] = "iniparser"
        self.cpp_info.components["iniparser"].names["cmake_find_package"] = "iniparser"
        self.cpp_info.components["iniparser"].names["cmake_find_package_multi"] = "iniparser"
        self.cpp_info.components["iniparser"].set_property("cmake_target_name", "iniparser::iniparser")
        self.cpp_info.components["iniparser"].set_property("pkg_config_name", "iniparser")
