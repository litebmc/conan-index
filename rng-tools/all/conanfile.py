import os
from conan import ConanFile
from conan.tools.layout import basic_layout
from conan.tools.gnu import AutotoolsToolchain, Autotools
from conan.tools.scm import Git
from conan.tools.files import chdir, get

class RngToolsConan(ConanFile):
    name = "rng-tools"
    url = "https://github.com/nhorman/rng-tools.git"
    homepage = "https://github.com/nhorman/rng-tools.git"
    license = "GPL-2.0"
    description = ("This is a random number generator daemon.")

    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "AutotoolsToolchain", "PkgConfigDeps"

    def layout(self):
        basic_layout(self)

    def requirements(self):
        self.requires("libxml2/[>=2.12.7]")
        self.requires("libcap/2.70")
        self.requires("openssl/[>=3.0.0]")

    def source(self):
        get(self, **self.conan_data["sources"][self.version],
            destination=self.source_folder, strip_root=True)

    def generator(self):
        tc = AutotoolsToolchain(self)
        tc.generate()

    def build(self):
        with chdir(self, self.source_folder):
            print(os.environ.get("PATH"))
            self.run("./autogen.sh")

        args = ["--without-pkcs11", "--without-nistbeacon", "--without-qrypt", "--without-rtlsdr"]
        autotools = Autotools(self)
        autotools.configure(args=args)
        autotools.make()
        autotools.install()

    def package(self):
        pass
