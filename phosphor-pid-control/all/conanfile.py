"""phosphor-pid-control构建脚本"""
import os
from conan import ConanFile
from conan.tools.meson import MesonToolchain, Meson
from conan.tools.layout import basic_layout
from conan.tools.scm import Git
from conan.tools import files

required_conan_version = ">=2.0.5"

# conan create . -pr litebmc.ini -pr:b default --version=2.14.0
class OBmcConan(ConanFile):
    name = "phosphor-pid-control"
    url = "https://github.com/openbmc/phosphor-pid-control.git"
    license = "Apache 2.0"
    description = ("OpenBMC PID-based Thermal Control Daemon.")
    settings = "os", "arch", "compiler", "build_type"
    extension_properties = {
        "compatibility_cppstd": False,
        "compatibility_cstd": False
    }
    generators = "PkgConfigDeps", "VirtualBuildEnv"
    options = {
        "oe_sdk": [True, False],
        "strict_failsafe_pwm": [True, False],
        "offline_failsafe_pwm": [True, False],
        "systemd_target": ["ANY"],
        "unc_failsafe": [True, False],
    }
    default_options = {
        "oe_sdk": False,
        "strict_failsafe_pwm": False,
        "offline_failsafe_pwm": False,
        "systemd_target": "multi-user.target",
        "unc_failsafe": False,
    }

    def export_sources(self):
        files.export_conandata_patches(self)
        files.copy(self, "rootfs/*", self.recipe_folder, self.export_sources_folder)

    def layout(self):
        basic_layout(self)
        self.cpp.package.resdirs = ["usr/share"]
        self.cpp.package.bindirs = ["usr/bin"]

    def source(self):
        """下载源码"""
        Git(self).fetch_commit(**self.conan_data["sources"][self.version])

    def requirements(self):
        requires = self.conan_data.get("requires", {}).get(self.version, [])
        for req in requires:
            self.requires(req)

    def build_requirements(self):
        self.tool_requires('pkgconf/2.0.3')

    def generate(self):
        tc = MesonToolchain(self)
        if self.settings.build_type == "MinSizeRel":
            tc.project_options["strip"] = True
        tc.project_options["tests"] = "disabled"
        tc.project_options["oe-sdk"] = ("enabled" if self.options.oe_sdk else "disabled")
        tc.project_options["strict-failsafe-pwm"] = (True if self.options.strict_failsafe_pwm else False)
        tc.project_options["offline-failsafe-pwm"] = (True if self.options.offline_failsafe_pwm else False)
        tc.project_options["systemd_target"] = str(self.options.systemd_target)
        tc.project_options["unc-failsafe"] = (True if self.options.unc_failsafe else False)
        tc.generate()

    def build(self):
        files.apply_conandata_patches(self)
        meson = Meson(self)
        meson.configure()
        meson.build()
        meson.install()

    def package(self):
        files.copy(self, "*", os.path.join(self.export_sources_folder, "rootfs"), self.package_folder)
        pass

    def package_info(self):
        pass
